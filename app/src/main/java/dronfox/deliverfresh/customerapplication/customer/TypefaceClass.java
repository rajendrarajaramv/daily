package dronfox.deliverfresh.customerapplication.customer;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.widget.TextView;

/**
 * Created by Pardeep on 12/5/2015.
 */
public class TypefaceClass
{
    Typeface typeface;
    public Typeface functionType(Context context)
    {
        typeface=Typeface.createFromAsset(context.getAssets(),"mons.otf");
        return typeface;
    }


    public void setShodow(TextView title)
    {
        title.setShadowLayer(1.0f, 1.0f, 1.0f, Color.BLACK);
    }
}
