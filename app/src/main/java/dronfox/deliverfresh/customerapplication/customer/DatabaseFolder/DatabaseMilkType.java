package dronfox.deliverfresh.customerapplication.customer.DatabaseFolder;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;


import java.util.ArrayList;

/**
 * Created by Pardeep on 11/13/2015.
 */
public class DatabaseMilkType
{
    SQLiteDatabase sqLiteDatabase;
    Context  context;
    public  DatabaseMilkType(Context ctx)
    {
        context=ctx;
        sqLiteDatabase=context.openOrCreateDatabase("DeliverFresh",context.MODE_PRIVATE,null);
  createTableMilk();
    }

    public void createTableMilk()
    {

        String create="CREATE TABLE IF NOT EXISTS milkDetails(category VARCHAR,Brandname VARCHAR," +
                "BrandVariety VARCHAR,BrandPrice VARCHAR," +
                "availCity VARCHAR,availArea VARCHAR,imagePath VARCHAR,catPic VARCHAR)";
    sqLiteDatabase.execSQL(create);

    }



    public void InsertInDatabase(String category,String Brandname,
                                 String BrandVariety,String BrandPrice,
                                 String availCity,String availArea,String imagePath,String catPic)
    {
   String insert="INSERT INTO milkDetails VALUES('"+category+"'," +
           "'"+Brandname+"','"+BrandVariety+"'," +
           "'"+BrandPrice+"','"+availCity+"'," +
           "'"+availArea+"','"+imagePath+"','"+catPic+"')";
    sqLiteDatabase.execSQL(insert);
    }



    public  void Delete()
    {
        String delete="DELETE FROM milkDetails";
        sqLiteDatabase.execSQL(delete);
    }




    public int getSize()
    {
        String get="SELECT * FROM milkDetails";
        Cursor cursor=sqLiteDatabase.rawQuery(get,null);
        return cursor.getCount();
    }

    public void ShowAllCategory(ArrayList<String> milkType,ArrayList<String> milkImage)
    {
        milkType.clear();
        milkImage.clear();
        String select="SELECT DISTINCT category FROM milkDetails";
        Cursor cursor=sqLiteDatabase.rawQuery(select,null);
        if(cursor.getCount() < 0)
        {

        }
        else
        {
            cursor.moveToFirst();
            do
            {
                String categroies=cursor.getString(cursor.getColumnIndex("category"));
//String image=cursor.getString(cursor.getColumnIndex("imagePath"));


                String pic="";
 String selectPic="SELECT  catPic from milkDetails WHERE category='"+categroies+"'";
                Cursor cursor1=sqLiteDatabase.rawQuery(selectPic,null);
                if(cursor1.getCount() < 1)
                {

                }
                else
                {
                    cursor1.moveToFirst();
                    do
                    {
                pic    =cursor1.getString(cursor1.getColumnIndex("catPic"));




                    }
                    while (cursor1.moveToNext());
                }
                milkImage.add(pic);

                milkType.add(categroies);
  //              milkImage.add(image);
            }
            while (cursor.moveToNext());
        }


    }




    public void getBrandName(String category,ArrayList<String> list)
    {
        list.clear();
        String select="SELECT DISTINCT Brandname from milkDetails where category='"+category+"'";
        Cursor cursor=sqLiteDatabase.rawQuery(select,null);
        if(cursor.getCount() < 1)
        {

        }
        else
        {
            cursor.moveToFirst();
            do
            {
            String getBrand=cursor.getString(cursor.getColumnIndex("Brandname"));
                list.add(getBrand);
            }
            while (cursor.moveToNext());
        }
    }


    public void getBrandVariety(String category,String Brandname,ArrayList<String> listvariety,ArrayList<String> listPrice,ArrayList<String> arrayListImage)
    {
        listvariety.clear();
   listPrice.clear();
        arrayListImage.clear();
        String select="SELECT imagePath,BrandVariety,BrandPrice from milkDetails where Brandname='"+Brandname+"' AND category='"+category+"'";
        Cursor cursor=sqLiteDatabase.rawQuery(select,null);
        if(cursor.getCount() < 1)
        {

        }
        else
        {
            cursor.moveToFirst();
            do
            {
                String getImage=cursor.getString(cursor.getColumnIndex("imagePath"));


                String getBrand=cursor.getString(cursor.getColumnIndex("BrandVariety"));
                String getPrice=cursor.getString(cursor.getColumnIndex("BrandPrice"));
listPrice.add(getPrice);

                arrayListImage.add(getImage);
                listvariety.add(getBrand);
            }
            while (cursor.moveToNext());
        }
    }


}
