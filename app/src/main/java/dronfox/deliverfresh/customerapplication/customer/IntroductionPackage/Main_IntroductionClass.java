package dronfox.deliverfresh.customerapplication.customer.IntroductionPackage;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import dronfox.deliverfresh.customerapplication.customer.CirclePageIndicator;
import dronfox.deliverfresh.customerapplication.customer.R;

/**
 * Created by Pardeep on 11/15/2015.
 */
public class Main_IntroductionClass extends AppCompatActivity
{
    ViewPager viewPager;
    Adapter_ViewPager adapter_viewPager;
CirclePageIndicator circlePageIndicator;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
setContentView(R.layout.intro_main);

        viewPager=(ViewPager)findViewById(R.id.intro_viewpager);
adapter_viewPager=new Adapter_ViewPager(getSupportFragmentManager());
        viewPager.setAdapter(adapter_viewPager);

        circlePageIndicator = (CirclePageIndicator)findViewById(R.id.titles);
        circlePageIndicator.setViewPager(viewPager);


    }

}
