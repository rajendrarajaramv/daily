package dronfox.deliverfresh.customerapplication.customer;

import com.squareup.okhttp.OkHttpClient;

import java.util.concurrent.TimeUnit;

import retrofit.RestAdapter;
import retrofit.client.OkClient;

/**
 * Created by Pardeep on 1/12/2016.
 */
public class MainRetrofit
{
    RetrofitInterface retrofitInterface;
    RestAdapter restAdapter;
OkHttpClient okHttpClient;

    public RetrofitInterface functionRetro()
    {
        okHttpClient=new OkHttpClient();
        okHttpClient.setConnectTimeout(30, TimeUnit.SECONDS);
        okHttpClient.setReadTimeout(30, TimeUnit.SECONDS);

        restAdapter=new RestAdapter.Builder().setEndpoint("http://deliverfresh.in").setClient(new OkClient(okHttpClient)).build();
        restAdapter.setLogLevel(RestAdapter.LogLevel.FULL);

        retrofitInterface=restAdapter.create(RetrofitInterface.class);
  return retrofitInterface;

    }


}
