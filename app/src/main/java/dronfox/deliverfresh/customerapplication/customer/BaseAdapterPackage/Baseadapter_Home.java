package dronfox.deliverfresh.customerapplication.customer.BaseAdapterPackage;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import dronfox.deliverfresh.customerapplication.customer.Navigation_Fragments.Fragment_MilkDetails;
import dronfox.deliverfresh.customerapplication.customer.R;
import dronfox.deliverfresh.customerapplication.customer.TypefaceClass;

/**
 * Created by Pardeep on 10/20/2015.
 */
public class Baseadapter_Home extends RecyclerView.Adapter<Baseadapter_Home.ViewHolder>
{
    Context context;
    ArrayList<String> listName;
    ArrayList<String> listCategoryName;
    ArrayList<String> listPic;
TypefaceClass typefaceClass;
    public Baseadapter_Home(Context ctx,ArrayList<String>name,ArrayList<String>pic)
    {
        context=ctx;
        listName=name;
        listPic=pic;
        typefaceClass=new TypefaceClass();
     }
    @Override
    public Baseadapter_Home.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View inflaterview= LayoutInflater.from(context).inflate(R.layout.baseadapter_home, parent, false);
        ViewHolder vh=new ViewHolder(inflaterview);


        return vh;
    }

    @Override
    public void onBindViewHolder(final Baseadapter_Home.ViewHolder holder, final int position) {
     Picasso.with(context).load(listPic.get(position)).into(holder.productImage, new Callback() {

         @Override
         public void onSuccess() {
             holder.progressBar.setVisibility(View.GONE);
         }

         @Override
         public void onError() {

         }
     });

//holder.productImage.setImageResource(R.drawable.milk);
        holder.textProductName.setTypeface(typefaceClass.functionType(context));
        holder.textProductName.setText("" + listName.get(position));
holder.view3.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {


            Fragment fragment=new Fragment_MilkDetails();
            Bundle bundle=new Bundle();
            bundle.putString("category",""+listName.get(position));
            fragment.setArguments(bundle);
            FragmentTransaction tranc = ((FragmentActivity) context).getSupportFragmentManager().beginTransaction();
            tranc.replace(R.id.content_frame, fragment).addToBackStack(null);
            tranc.commit();
     }
});
    }

    @Override
    public int getItemCount() {
        return listName.size();
    }


    public class ViewHolder extends  RecyclerView.ViewHolder
    {
ProgressBar progressBar;
        TextView textProductName;
        ImageView productImage;
CardView view3;
        public ViewHolder(View itemView) {
            super(itemView);
textProductName=(TextView)itemView.findViewById(R.id.productText);
            productImage=(ImageView)itemView.findViewById(R.id.productImage);
            view3=(CardView)itemView.findViewById(R.id.view3);
        progressBar=(ProgressBar)itemView.findViewById(R.id.progressBar);
        }
    }
}
