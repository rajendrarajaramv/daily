package dronfox.deliverfresh.customerapplication.customer.Navigation_Fragments;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import dronfox.deliverfresh.customerapplication.customer.MainRetrofit;
import dronfox.deliverfresh.customerapplication.customer.R;
import dronfox.deliverfresh.customerapplication.customer.SharedPrefrencesPackage.SharedPrefClass;
import dronfox.deliverfresh.customerapplication.customer.TypefaceClass;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Pardeep on 11/8/2015.
 */
public class Fragment_Account extends Fragment
{
    Button deleteButton;
MainRetrofit mainRetrofit;


ImageView imgEdit;


    TextView titleTool;

    TextView profileContact;
    TextView profileCity;
    TextView profilePlace;

    SharedPrefClass sharedPrefClass;

    String password;
    TextView profilePassword;
TypefaceClass typefaceClass;
TextView passwordText;
    TextView personaltext;
    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_acount,container,false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowCustomEnabled(false);




        imgEdit=(ImageView)view.findViewById(R.id.imageEdit);
mainRetrofit=new MainRetrofit();

        typefaceClass=new TypefaceClass();
deleteButton=(Button)view.findViewById(R.id.deleteAccount);

        titleTool=(TextView)view.findViewById(R.id.titleTool);


        personaltext=(TextView)view.findViewById(R.id.personaltext);
        personaltext.setTypeface(typefaceClass.functionType(getActivity()));

        passwordText=(TextView)view.findViewById(R.id.passwordText);
        passwordText.setTypeface(typefaceClass.functionType(getActivity()));
        profileContact=(TextView)view.findViewById(R.id.profile_contact);
        profileCity=(TextView)view.findViewById(R.id.profileCity);
        profilePlace=(TextView)view.findViewById(R.id.profilePlace);



        profilePassword=(TextView)view.findViewById(R.id.profilePassword);
        sharedPrefClass=new SharedPrefClass(getActivity());



        titleTool.setText("" + sharedPrefClass.getName());

        profileContact.setText("" + sharedPrefClass.getContactNumber());
        profileCity.setText("" + sharedPrefClass.getState());
        profilePlace.setText("" + sharedPrefClass.getPlace());

        profilePassword.setText("" + sharedPrefClass.getPassword());


        titleTool.setTypeface(typefaceClass.functionType(getActivity()));
        profileContact.setTypeface(typefaceClass.functionType(getActivity()));
        profileCity.setTypeface(typefaceClass.functionType(getActivity()));
        profilePlace.setTypeface(typefaceClass.functionType(getActivity()));
        profilePassword.setTypeface(typefaceClass.functionType(getActivity()));

        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder deleteAcount = new AlertDialog.Builder(getActivity());
                deleteAcount.setTitle("Change Password");
                deleteAcount.setMessage("Do you really want to delete Account");
                deleteAcount.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mainRetrofit.functionRetro().DeleteAcount("" + sharedPrefClass.getContactNumber(), new Callback<String>() {
                            @Override
                            public void success(String s, Response response) {
                                if (s.equals("delete")) {
                                    sharedPrefClass.setLogin("loginscreen");
                                    getActivity().finish();
                                }
                            }

                            @Override
                            public void failure(RetrofitError retrofitError) {

                            }
                        });

                        //  sharedPrefClass.setLogin("false");
                        //getActivity().finish();

                    }
                });
                deleteAcount.create();
                deleteAcount.show();
            }
        });


        imgEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Change Password");
                LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(((AppCompatActivity) getActivity()).LAYOUT_INFLATER_SERVICE);
                View view1 = inflater.inflate(R.layout.editpassword, null);

                EditText editText = (EditText) view1.findViewById(R.id.currentPassword);
                final EditText editNewPassword = (EditText) view1.findViewById(R.id.newPassword);
                editText.setText("" + password);
                builder.setView(view1);


                builder.setPositiveButton("Update", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {


                        final ProgressDialog progressDialog=new ProgressDialog(getActivity());
                        progressDialog.setMessage("Please Wait...");
                        progressDialog.show();
                        mainRetrofit.functionRetro().updatePassword(editNewPassword.getText().toString(), "" + sharedPrefClass.getContactNumber(), new Callback<String>() {
                            @Override
                            public void success(String s, Response response) {
                                if(s.equals("update"))
                                {
                                    progressDialog.dismiss();
                                    profilePassword.setText(""+editNewPassword.getText().toString());
                                }
                            }

                            @Override
                            public void failure(RetrofitError retrofitError) {

                            }
                        });

                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });


                builder.create();


                builder.show();

            }
        });


        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    getFragmentManager().popBackStackImmediate();
                }
                return false;
            }
        });

        return  view;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
    menu.clear();
        super.onCreateOptionsMenu(menu, inflater);
    }
}
