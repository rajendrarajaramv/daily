package dronfox.deliverfresh.customerapplication.customer.IntroductionPackage;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by Pardeep on 11/15/2015.
 */
public class Adapter_ViewPager extends FragmentStatePagerAdapter
{
    public Adapter_ViewPager(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

      switch (position)
      {
          case 0:
              return  new Fragment_One();
          case 1:
              return  new Fragment_Two();
          case 2:
              return  new Fragment_Three();
          case 3:
              return  new Fragment_Four();
      }

        return null;
    }

    @Override
    public int getCount() {
        return 4;
    }
}
