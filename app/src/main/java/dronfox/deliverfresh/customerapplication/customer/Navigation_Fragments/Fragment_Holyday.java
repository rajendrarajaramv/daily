package dronfox.deliverfresh.customerapplication.customer.Navigation_Fragments;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import dronfox.deliverfresh.customerapplication.customer.MainRetrofit;
import dronfox.deliverfresh.customerapplication.customer.PojoClass;
import dronfox.deliverfresh.customerapplication.customer.R;
import dronfox.deliverfresh.customerapplication.customer.SharedPrefrencesPackage.SharedPrefClass;
import dronfox.deliverfresh.customerapplication.customer.TypefaceClass;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Pardeep on 11/8/2015.
 */
public class Fragment_Holyday extends Fragment implements View.OnClickListener {



    ImageView imgSchedule;
    Button btnSchedule;
    boolean regular_bit = false;
    String isRegular;
    String finalTomorrowDate;
    EditText edtNumDays;
    String startDate;
    String endDate;
    TextView num;
    String[] dateFrom;
    String[] dateTo;


    Date dateNow;
String nowDate;



    int remain;
    int usedDays;
    int totalDayOrder;
    LinearLayout hideLayoutHoli;
    LinearLayout showLayoutHoli;
    int refundDays;

    String getOrderId;
MainRetrofit mainRetrofit;
    String month[] = {"", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"};

    TextView orderTo;
    TextView orderFrom;
    TableRow noti;
    SharedPrefClass sharedPrefClass;
TextView orderRemaining;
    TypefaceClass typefaceClass;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.holyday, container, false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowCustomEnabled(false);

       mainRetrofit=new MainRetrofit();
typefaceClass=new TypefaceClass();

        sharedPrefClass = new SharedPrefClass(getActivity());
        num = (TextView) view.findViewById(R.id.num);
        orderRemaining=(TextView)view.findViewById(R.id.orderRemaining);
        orderFrom = (TextView) view.findViewById(R.id.orderFrom);
        orderTo = (TextView) view.findViewById(R.id.orderTo);
        noti = (TableRow) view.findViewById(R.id.noti);
        noti.setVisibility(View.GONE);




        num.setTypeface(typefaceClass.functionType(getActivity()));

        orderRemaining.setTypeface(typefaceClass.functionType(getActivity()));

        orderFrom.setTypeface(typefaceClass.functionType(getActivity()));

        orderTo.setTypeface(typefaceClass.functionType(getActivity()));



        String extendedDates;
        edtNumDays = (EditText) view.findViewById(R.id.edtNumDays);
        edtNumDays.setVisibility(View.GONE);
        imgSchedule = (ImageView) view.findViewById(R.id.imgSchedule);
        imgSchedule.setOnClickListener(this);
        btnSchedule = (Button) view.findViewById(R.id.btnSchedule);
        btnSchedule.setOnClickListener(this);



        hideLayoutHoli = (LinearLayout) view.findViewById(R.id.hideLayoutHoli);

        showLayoutHoli = (LinearLayout) view.findViewById(R.id.showLayoutHoli);


        hideLayoutHoli.setVisibility(View.GONE);

        try {
            getOrderDates();
        }catch (Exception e)
        {

        }
            edtNumDays.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (s.length() < 1) {
                    noti.setVisibility(View.GONE);
                } else {
                    noti.setVisibility(View.VISIBLE);
                    num.setText("" + s);
                }

            }
        });



        return view;

    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.imgSchedule:
                edtNumDays.setVisibility(View.VISIBLE);
                break;


            case R.id.btnSchedule:
                if (regular_bit == false) {
                    Snackbar.make(getActivity().findViewById(android.R.id.content), "Cannot Place holidays While Non Regular Orders", Snackbar.LENGTH_LONG).show();
                } else if (edtNumDays.getText().toString().equals("")) {
                    Snackbar.make(getActivity().findViewById(android.R.id.content), "Please Specify the number of Days", Snackbar.LENGTH_LONG).show();

                } else {
                    final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle("What After You Back");
                    builder.setMessage("We have noticed that you are going on " + num.getText().toString() + " Days Holidays, So Do you want to extend the last date of the order?");

                    builder.setPositiveButton("Yes I want to extend", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

//
//                            Log.e("====", "" + startDate + "," + endDate);
//
//
                            final String newStartDate = IncreamentDates(Integer.parseInt(num.getText().toString()), nowDate, "yyyy-MM-dd");




                            AlertDialog.Builder confirm=new AlertDialog.Builder(getActivity());
                            confirm.setTitle("Holiday Confirmed");
                            confirm.setMessage("Because you have extended the order and going to holidays we will deliver you order from " + newStartDate + " to " + IncreamentDates(remain, newStartDate, "yyyy-MM-dd"));
                            confirm.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {


                            mainRetrofit.functionRetro().wantToExtend(sharedPrefClass.getContactNumber(),
                                    newStartDate, IncreamentDates(remain, newStartDate, "yyyy-MM-dd"),
                                    getOrderId, new Callback<String>() {
                                        @Override
                                        public void success(String s, Response response) {

                                        }

                                        @Override
                                        public void failure(RetrofitError retrofitError) {

                                        }
                                    });


                                }
                            });
                            confirm.setNegativeButton("Lemme Re-Schedule", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });
                            confirm.create();
                            confirm.show();


                        }
                    });
                    builder.create();
                    builder.show();


                    break;
                }
        }
    }

    public void getOrderDates()
    {

        final ProgressDialog progressDialog=new ProgressDialog(getActivity());
        progressDialog.setMessage("Fetching Upcoming Orders");
        progressDialog.show();
        mainRetrofit.functionRetro().showMyOrders(sharedPrefClass.getContactNumber(), ConverDateTommorow("yyyy-MM-dd"), new Callback<List<PojoClass>>() {
            @Override
            public void success(List<PojoClass> pojoClasses, Response response) {
                for (int i = 0; i < pojoClasses.size(); i++) {
                    startDate = pojoClasses.get(i).getOrderDate();
                    endDate = pojoClasses.get(i).getOrderEnd();
                    getOrderId = pojoClasses.get(i).getOrderId();
                    isRegular = pojoClasses.get(i).getIsRegular();

                }





                if(endDate.equals(""))
                {
                  AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle("Cannot Place Holiday");
                    builder.setMessage("You are about to set holiday for a tomorrow based order, Please Set holiday for the Daily based Order");
builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
    @Override
    public void onClick(DialogInterface dialog, int which) {

    }
});
                    builder.create();
                    builder.show();

                }
                else {


                dateFrom = startDate.split("-");
                dateTo = endDate.split("-");
                orderFrom.setText(dateFrom[2] + "\n" + month[Integer.parseInt("" + dateFrom[1])]);
                try {
                    orderTo.setText("" + dateTo[2] + "\n" + month[Integer.parseInt("" + dateTo[1])]);
                } catch (Exception e) {
                    orderTo.setText("--");
                }
                if (isRegular.equals("Non Regular")) {
                    regular_bit = false;
                } else {
                    regular_bit = true;
                }
                totalDayOrder=Integer.parseInt(""+daysBetween(convertStringIntoDate(startDate),convertStringIntoDate(endDate)))+1;

                 nowDate=new SimpleDateFormat("yyyy-MM-dd").format(new Date());


           usedDays= daysBetween(convertStringIntoDate(startDate), convertStringIntoDate(IncreamentDates(1,nowDate,"yyyy-MM-dd")));



                remain=totalDayOrder-usedDays;

                orderRemaining.setText(""+remain);



//


                    //              Log.e("Total Days Remaining",""+remainingDaysOrder);
                }

                progressDialog.dismiss();

            }

            @Override
            public void failure(RetrofitError retrofitError) {
                progressDialog.dismiss();
                hideLayoutHoli.setVisibility(View.VISIBLE);
                showLayoutHoli.setVisibility(View.GONE);
            }
        });
    }





    public String ConverDateTommorow(String Format)
    {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Format);




        Calendar calendar = Calendar.getInstance();
        Date today = calendar.getTime();
        calendar.setTime(today);
        calendar.add(Calendar.DAY_OF_YEAR, 1);
        Date tomorrow = calendar.getTime();
        DateFormat dateFormat = new SimpleDateFormat(Format);
        finalTomorrowDate = dateFormat.format(tomorrow);


        return finalTomorrowDate;


    }



    public Date convertStringIntoDate(String stirng)
    {
        Date date=null;
        try {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

            date = dateFormat.parse(stirng);
        }
        catch (Exception e)
        {

        }
return date;
    }







    public int daysBetween(Date d1, Date d2){
        return (int)( (d2.getTime() - d1.getTime()) / (1000 * 60 * 60 * 24));
    }





    public String IncreamentDates(int numDays,String setDate,String format)
    {

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        Calendar calendar = Calendar.getInstance();
        try {
            Date today = simpleDateFormat.parse(setDate);

            calendar.setTime(today);
            calendar.add(Calendar.DAY_OF_YEAR, numDays);
        }catch (Exception e)
        {

        }

        Date tomorrow = calendar.getTime();
        DateFormat dateFormat = new SimpleDateFormat(format);
        String increamentDate = dateFormat.format(tomorrow);

        return increamentDate;
    }



}