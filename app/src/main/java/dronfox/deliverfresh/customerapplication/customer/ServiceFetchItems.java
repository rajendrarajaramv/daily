package dronfox.deliverfresh.customerapplication.customer;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;


import java.util.List;

import dronfox.deliverfresh.customerapplication.customer.DatabaseFolder.DatabaseMilkType;
import dronfox.deliverfresh.customerapplication.customer.Navigation_Fragments.Fragement_Home;
import dronfox.deliverfresh.customerapplication.customer.SharedPrefrencesPackage.SharedPrefClass;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Pardeep on 1/16/2016.
 */
public class ServiceFetchItems extends Service
{
    DatabaseMilkType databaseMilkType;
    MainRetrofit mainRetrofit;
SharedPrefClass sharedPrefClass;

    Fragement_Home fragement_home;
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public void onCreate() {
        super.onCreate();

        sharedPrefClass=new SharedPrefClass(this);


mainRetrofit=new MainRetrofit();

        databaseMilkType=new DatabaseMilkType(this);
functionFetchAllBrands(""+sharedPrefClass.getPlace(),""+sharedPrefClass.getState());

    }



    public void functionFetchAllBrands(String area,String city)
    {

        databaseMilkType.Delete();

//if(databaseMilkType.getSize() < 1) {
        mainRetrofit.functionRetro().MilkDetails(city, area, new Callback<List<PojoClass>>() {
            @Override
            public void success(List<PojoClass> pojoClasses, Response response) {
                for (int i = 0; i < pojoClasses.size(); i++) {
                    String category = pojoClasses.get(i).getCatName();
                    String catPic = pojoClasses.get(i).getCatPic();
                    String Brandname = pojoClasses.get(i).getBrandname();
                    String BrandVariety = pojoClasses.get(i).getBrandVariety();
                    String BrandPrice = pojoClasses.get(i).getBrandPrice();
                    String availCity = pojoClasses.get(i).getAvailCity();
                    String availArea = pojoClasses.get(i).getAvailArea();
                    String imagepath = pojoClasses.get(i).getImagePath();


                    databaseMilkType.InsertInDatabase(category, Brandname, BrandVariety, BrandPrice, availCity, availArea, imagepath, catPic);
                }
                if(Fragement_Home.getInstance()!=null) {
                    Fragement_Home.getInstance().UpdateList();
                    stopSelf();
                }


            }

            @Override
            public void failure(RetrofitError retrofitError) {
            }
        });


    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
