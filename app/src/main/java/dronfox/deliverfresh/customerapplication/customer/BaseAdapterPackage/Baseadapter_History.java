package dronfox.deliverfresh.customerapplication.customer.BaseAdapterPackage;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import java.util.ArrayList;

import dronfox.deliverfresh.customerapplication.customer.DatabaseFolder.EditOrdersDatabase;
import dronfox.deliverfresh.customerapplication.customer.R;
import dronfox.deliverfresh.customerapplication.customer.SharedPrefrencesPackage.SharedPrefClass;
import dronfox.deliverfresh.customerapplication.customer.TypefaceClass;

/**
 * Created by Pardeep on 10/20/2015.
 */
public class Baseadapter_History extends RecyclerView.Adapter<Baseadapter_History.ViewHolder>
{
    Context context;

    ArrayList<String> listOrderId;
    ArrayList<String> listOrderDate;
    ArrayList<String> listOrderEnd;

    ArrayList<String> listOrderBrandName;
    ArrayList<String> listOrderVariety;
    ArrayList<String> listOrderQaunt;
EditOrdersDatabase editOrdersDatabase;

Display display;
TypefaceClass typefaceClass;
    public Baseadapter_History(Context ctx,ArrayList<String> orderDate,ArrayList<String> orderEnd)
    {
        context=ctx;
        listOrderDate=orderDate;
        listOrderEnd=orderEnd;
editOrdersDatabase=new EditOrdersDatabase(context);


        listOrderBrandName=new ArrayList<String>();
        listOrderVariety=new ArrayList<String>();
        listOrderQaunt=new ArrayList<String>();
typefaceClass=new TypefaceClass();
    }
    @Override
    public Baseadapter_History.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View inflaterview= LayoutInflater.from(context).inflate(R.layout.baseadapter_history, parent, false);
        ViewHolder vh=new ViewHolder(inflaterview);


        return vh;
    }

    @Override
    public void onBindViewHolder(Baseadapter_History.ViewHolder holder, final int position) {
        holder.textDate.setText("" + listOrderDate.get(position) + " - " + listOrderEnd.get(position));

        holder.textDate.setTypeface(typefaceClass.functionType(context));
        holder.cardviewTommorow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WindowManager windowManager = (WindowManager) context.getSystemService(context.WINDOW_SERVICE);
                display = windowManager.getDefaultDisplay();
                final int height = display.getHeight() / 2;

                        final Dialog dig = new Dialog(context);
                        dig.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                        dig.setContentView(R.layout.dialogbottom);
                        dig.getWindow().setGravity(Gravity.BOTTOM);
                        dig.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        dig.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, height);
                LinearLayoutManager linearLayoutManager=new LinearLayoutManager(context);
                RecyclerView recyclerView=(RecyclerView)dig.findViewById(R.id.recyclerItemDeliver);
                recyclerView.setLayoutManager(linearLayoutManager);
                recyclerView.setAdapter(new Baseadapter_HistoryOrders(context, listOrderBrandName, listOrderVariety, listOrderQaunt));

                TextView title=(TextView)dig.findViewById(R.id.title);
                title.setText(""+new SharedPrefClass(context).getName());
title.setTypeface(typefaceClass.functionType(context));


editOrdersDatabase.FetchAllOrders(listOrderDate.get(position), listOrderBrandName, listOrderVariety, listOrderQaunt);
                FloatingActionButton floatView=(FloatingActionButton)dig.findViewById(R.id.floatView);
                floatView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dig.dismiss();
                    }
                });
                recyclerView.setAdapter(new Baseadapter_HistoryOrders(context,listOrderBrandName,listOrderVariety,listOrderQaunt));



dig.show();
                    }
                });
            }


    @Override
    public int getItemCount() {

        return listOrderDate.size();
    }


    public class ViewHolder extends  RecyclerView.ViewHolder
    {

        CardView cardviewTommorow;
        TextView textDate;
        TextView orderList;
        public ViewHolder(View itemView) {

            super(itemView);

            cardviewTommorow=(CardView)itemView.findViewById(R.id.cardviewTommorow);
        textDate=(TextView)itemView.findViewById(R.id.textDate);
        }
    }
}
