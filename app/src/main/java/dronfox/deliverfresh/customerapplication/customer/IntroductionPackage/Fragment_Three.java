package dronfox.deliverfresh.customerapplication.customer.IntroductionPackage;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import dronfox.deliverfresh.customerapplication.customer.R;
import dronfox.deliverfresh.customerapplication.customer.TypefaceClass;

/**
 * Created by Pardeep on 11/15/2015.
 */
public class Fragment_Three extends Fragment
{


    TypefaceClass typefaceClass;
    TextView payTitle;
    TextView payDesc;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {



        View view=inflater.inflate(R.layout.intro_three,container,false);



        typefaceClass=new TypefaceClass();
        payTitle=(TextView)view.findViewById(R.id.payTitle);
        payDesc=(TextView)view.findViewById(R.id.payDesc);
        payTitle.setTypeface(typefaceClass.functionType(getActivity()));

        payDesc.setTypeface(typefaceClass.functionType(getActivity()));

        typefaceClass.setShodow(payTitle);
        typefaceClass.setShodow(payDesc);


        return  view;
    }

}
