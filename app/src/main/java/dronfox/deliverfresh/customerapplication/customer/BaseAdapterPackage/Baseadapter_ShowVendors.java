package dronfox.deliverfresh.customerapplication.customer.BaseAdapterPackage;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.ArrayList;

import dronfox.deliverfresh.customerapplication.customer.R;
import dronfox.deliverfresh.customerapplication.customer.TypefaceClass;

/**
 * Created by Pardeep on 10/20/2015.
 */
public class Baseadapter_ShowVendors extends RecyclerView.Adapter<Baseadapter_ShowVendors.ViewHolder>
{
    Context context;

    ArrayList<String> listOrderId;
    ArrayList<String> listOrderDate;
    ArrayList<String> listOrderEnd;



    Display display;
    TypefaceClass typefaceClass;
    public Baseadapter_ShowVendors(Context ctx,ArrayList<String> name,ArrayList<String> address,ArrayList<String> phone)
    {
        context=ctx;

        listOrderId=name;
        listOrderDate=address;
        listOrderEnd=phone;


        typefaceClass=new TypefaceClass();
    }
    @Override
    public Baseadapter_ShowVendors.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View inflaterview= LayoutInflater.from(context).inflate(R.layout.baseadapter_venders, parent, false);
        ViewHolder vh=new ViewHolder(inflaterview);


        return vh;
    }

    @Override
    public void onBindViewHolder(Baseadapter_ShowVendors.ViewHolder holder, final int position) {

holder.textVendorAddress.setText(""+listOrderDate.get(position));
        holder.textVendorName.setText(""+listOrderId.get(position));
        holder.layout_row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse("tel:" + listOrderEnd.get(position)));
                context.startActivity(intent);


            }
        });
    }


    @Override
    public int getItemCount() {

        return listOrderDate.size();
    }


    public class ViewHolder extends  RecyclerView.ViewHolder
    {

        TableRow layout_row;
        TextView textVendorName;
        TextView textVendorAddress;
        ImageView img;
        public ViewHolder(View itemView) {

            super(itemView);

            textVendorName=(TextView)itemView.findViewById(R.id.vendorName_txt);
            textVendorAddress=(TextView)itemView.findViewById(R.id.vendorName_address);
            layout_row=(TableRow)itemView.findViewById(R.id.layout_row);
            img=(ImageView)itemView.findViewById(R.id.imgPhone);


        }
    }
}
