package dronfox.deliverfresh.customerapplication.customer;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import dronfox.deliverfresh.customerapplication.customer.SharedPrefrencesPackage.SharedPrefClass;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Pardeep on 1/19/2016.
 */
public class OrderDel extends AppCompatActivity
{
    MainRetrofit mainRetrofit;
    Button button;
    SharedPrefClass sharedPrefClass;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    setContentView(R.layout.orderdel);
   mainRetrofit=new MainRetrofit();
    sharedPrefClass=new SharedPrefClass(this);
        button=(Button)findViewById(R.id.buttonDlei);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                mainRetrofit.functionRetro().semdOrder(sharedPrefClass.getPlace(),"Order is delivered to "+sharedPrefClass.getName()+"\n"+sharedPrefClass.getContactNumber(), new Callback<String>() {
                    @Override
                    public void success(String s, Response response) {
                        Toast.makeText(OrderDel.this,"Thanks For Your Support",Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void failure(RetrofitError retrofitError) {

                    }
                });


            }
        });
    }
}
