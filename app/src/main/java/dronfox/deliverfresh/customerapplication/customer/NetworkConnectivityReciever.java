package dronfox.deliverfresh.customerapplication.customer;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by Pardeep on 11/12/2015.
 */
public class NetworkConnectivityReciever extends BroadcastReceiver
{

    public static Boolean isConnectionAvail=false;
    @Override
    public void onReceive(Context context, Intent intent) {

        Connectivity(context);
    }

    public boolean Connectivity(Context context)
    {

        ConnectivityManager connectivity = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null)
        {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED)
                    {
                        isConnectionAvail=true;
                        return true;

                    }

        }
        isConnectionAvail=false;

    //    Toast.makeText(context, "Connection Not Available", Toast.LENGTH_LONG).show();
        return false;
    }

}
