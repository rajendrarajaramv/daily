package dronfox.deliverfresh.customerapplication.customer.BaseAdapterPackage;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import dronfox.deliverfresh.customerapplication.customer.DatabaseFolder.DbCounter;
import dronfox.deliverfresh.customerapplication.customer.MainActivity;
import dronfox.deliverfresh.customerapplication.customer.R;
import dronfox.deliverfresh.customerapplication.customer.TypefaceClass;

/**
 * Created by Pardeep on 10/20/2015.
 */
public class Baseadapter_BradnName extends RecyclerView.Adapter<Baseadapter_BradnName.ViewHolder>
{
    Context context;
    ArrayList<String> ItemName;
    ArrayList<String> ItemPrice;
    ArrayList<Integer> count;

int countNum=0;

    DbCounter  dbCounter;

    int getPrice;
int total;
    String brandNAME;
    String cat;
    TypefaceClass typefaceClass;
    String userTrue;
MainActivity mainActivity;
ArrayList<String> pic;
    public Baseadapter_BradnName(Context ctx,
                                 ArrayList<String> listItem,
                                 ArrayList<String> listPrice,
                                 ArrayList<Integer> listCount,
                                 String str,String strCat,ArrayList<String> listPic)
    {
        context=ctx;
ItemName=listItem;
        ItemPrice=listPrice;
  count=listCount;
 dbCounter=new DbCounter(context);
  brandNAME=str;
cat=strCat;
    dbCounter=new DbCounter(context);
pic=listPic;
        mainActivity=new MainActivity();
    typefaceClass=new TypefaceClass();
    }
    @Override
    public Baseadapter_BradnName.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View inflaterview= LayoutInflater.from(context).inflate(R.layout.baseadapter_raterice, parent, false);
        ViewHolder vh=new ViewHolder(inflaterview);


        return vh;
    }

    @Override
    public void onBindViewHolder(final Baseadapter_BradnName.ViewHolder holder, final int position) {

try {
    Picasso.with(context).load(pic.get(position)).into(holder.productImage);
}catch (Exception e)
{

}
        holder.productName.setText("" + ItemName.get(position));
        holder.productPrice.setText("₹" + ItemPrice.get(position));
holder.productName.setTypeface(typefaceClass.functionType(context));
        holder.productPrice.setTypeface(typefaceClass.functionType(context));



        holder.add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                WindowManager windowManager=(WindowManager)context.getSystemService(Context.WINDOW_SERVICE);
                Display metrics = windowManager.getDefaultDisplay();
                int width = metrics.getWidth();
                int height = metrics.getHeight();
                final Dialog dialog=new Dialog(context);
                dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

                dialog.setContentView(R.layout.dialogaddcart);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.getWindow().setLayout(width, height / 3);

                TextView itemName=(TextView)dialog.findViewById(R.id.cart_Itemname);
                TextView itemVar=(TextView)dialog.findViewById(R.id.cart_ItemVar);
                final TextView totalPrice=(TextView)dialog.findViewById(R.id.totalPrice);
                itemName.setText(""+brandNAME);
                itemVar.setText(""+ItemName.get(position));
                totalPrice.setText("0");
                getPrice=Integer.parseInt(""+ItemPrice.get(position));
                final TextView numCount=(TextView)dialog.findViewById(R.id.numCount);
                Button btnMinus=(Button)dialog.findViewById(R.id.btnMinus);
                Button btnPlus=(Button)dialog.findViewById(R.id.btnPlus);
                btnMinus.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if(countNum<1)
                        {

                        }
                        else
                        {
                            countNum--;
numCount.setText(""+countNum);

total=total-getPrice;
                            totalPrice.setText(" ₹ "+total);

                        }
                    }
                });


                btnPlus.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
           countNum++;
                        numCount.setText(""+countNum);
                    total=getPrice*countNum;
                    totalPrice.setText(" ₹" + total);

                    }
                });
                Button button=(Button)dialog.findViewById(R.id.doneMilk);
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


if(countNum<1)
{
       Toast.makeText(context, "Please Choose Atleast one Values", Toast.LENGTH_LONG).show();
    mainActivity.getInstance().UpdateCartIcon(dbCounter.ReturnSize());
} else {
    dbCounter.insertIntoDatabase(brandNAME, ItemName.get(position), "" + countNum, ItemPrice.get(position), "" + total, "1", cat);
    mainActivity.getInstance().UpdateCartIcon(dbCounter.ReturnSize());
}
                        countNum=0;


                        dialog.dismiss();
                    }
                });
                dialog.show();

            }
        });

    }

    @Override
    public int getItemCount() {
        return ItemName.size();
    }


    public class ViewHolder extends  RecyclerView.ViewHolder
    {
        TextView productName;
        TextView productPrice;
    TextView numCount;
        ImageView productImage;
Button add;
        public ViewHolder(final View itemView) {

            super(itemView);
            productName=(TextView)itemView.findViewById(R.id.productName);
            productPrice=(TextView)itemView.findViewById(R.id.productPrice);
       //     numCount=(TextView)itemView.findViewById(R.id.numCount);

add=(Button)itemView.findViewById(R.id.add);
productImage=(ImageView)itemView.findViewById(R.id.productImage);
        }
    }
}
