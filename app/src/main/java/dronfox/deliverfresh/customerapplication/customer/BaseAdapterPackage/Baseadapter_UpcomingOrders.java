package dronfox.deliverfresh.customerapplication.customer.BaseAdapterPackage;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import dronfox.deliverfresh.customerapplication.customer.MainRetrofit;
import dronfox.deliverfresh.customerapplication.customer.R;
import dronfox.deliverfresh.customerapplication.customer.SharedPrefrencesPackage.SharedPrefClass;
import dronfox.deliverfresh.customerapplication.customer.TypefaceClass;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Pardeep on 10/20/2015.
 */
public class Baseadapter_UpcomingOrders extends RecyclerView.Adapter<Baseadapter_UpcomingOrders.ViewHolder>
{
    Context context;

    ArrayList<String> listOrderId;
    ArrayList<String> listOrderBrandName;
    ArrayList<String> listOrderBrandVariety;
    ArrayList<String> listQaunt;
SharedPrefClass sharedPrefClass;
    MainRetrofit mainRetrofit;
   TypefaceClass typefaceClass;
    public Baseadapter_UpcomingOrders(Context ctx,ArrayList<String> id,ArrayList<String> Bname,
                                      ArrayList<String> Bver,ArrayList<String> Bqaunt)
    {
        context=ctx;
    listOrderId=id;
        listOrderBrandName=Bname;
        listOrderBrandVariety=Bver;
        listQaunt=Bqaunt;

        mainRetrofit=new MainRetrofit();
sharedPrefClass=new SharedPrefClass(context);
    typefaceClass=new TypefaceClass();
    }
    @Override
    public Baseadapter_UpcomingOrders.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View inflaterview= LayoutInflater.from(context).inflate(R.layout.baseadapter_upcomingorders, parent, false);
        ViewHolder vh=new ViewHolder(inflaterview);


        return vh;
    }

    @Override
    public void onBindViewHolder(Baseadapter_UpcomingOrders.ViewHolder holder, final int position) {

holder.up_ItemVer.setText(""+listOrderBrandName.get(position));

        holder.itemBrand.setText("" + listOrderBrandVariety.get(position));

        holder.bottom_itemQaunt.setText("" + listQaunt.get(position));


        holder.up_ItemVer.setTypeface(typefaceClass.functionType(context));
        holder.itemBrand.setTypeface(typefaceClass.functionType(context));
        holder.bottom_itemQaunt.setTypeface(typefaceClass.functionType(context));



        holder.imgEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder=new AlertDialog.Builder(context);



                builder.setTitle("Do you want to delete this order");
                builder.setMessage("Once you will delete the order,We Will Make A refund equal to that amount");
                builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {




String dateReal=new SimpleDateFormat("yyyy-MM-dd").format(new Date());

mainRetrofit.functionRetro().deleteOrders("" + listOrderId.get(position), "" + listOrderBrandName.get(position), ""
        + listOrderBrandVariety.get(position),sharedPrefClass.getContactNumber(),dateReal,""+sharedPrefClass.getPlace(), new Callback<String>() {
    @Override
    public void success(String s, Response response)
    {
        if(s.equals("delete"))
        {
            listOrderId.remove(position);
            listOrderBrandName.remove(position);
            listOrderBrandVariety.remove(position);
            listQaunt.remove(position);
            notifyItemRemoved(position);
            notifyItemRangeChanged(0,listOrderId.size());
        }
    }

    @Override
    public void failure(RetrofitError retrofitError) {

    }
});
                    }
               });
                builder.create();
                builder.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return listOrderBrandName.size();
    }


    public class ViewHolder extends  RecyclerView.ViewHolder
    {
        TextView up_ItemVer;
        TextView itemBrand;
        TextView bottom_itemQaunt;
        ImageView  imgEdit;

        public ViewHolder(View itemView) {
            super(itemView);



             up_ItemVer=(TextView)itemView.findViewById(R.id.up_ItemVer);
            itemBrand=(TextView)itemView.findViewById(R.id.itemBrand);
             bottom_itemQaunt=(TextView)itemView.findViewById(R.id.bottom_itemQaunt);
             imgEdit=(ImageView)itemView.findViewById(R.id.edtImage);

        }
    }


    public String increamentDate()
    {
        Calendar calendar = Calendar.getInstance();
        Date today = calendar.getTime();

        calendar.add(Calendar.DAY_OF_YEAR, 1);
        Date tomorrow = calendar.getTime();

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        String todayAsString = dateFormat.format(today);
        String getDate = dateFormat.format(tomorrow);
return getDate;
    }
}
