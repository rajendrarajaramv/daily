package dronfox.deliverfresh.customerapplication.customer.Navigation_Fragments;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import dronfox.deliverfresh.customerapplication.customer.MainRetrofit;
import dronfox.deliverfresh.customerapplication.customer.PojoClass;
import dronfox.deliverfresh.customerapplication.customer.R;
import dronfox.deliverfresh.customerapplication.customer.SharedPrefrencesPackage.SharedPrefClass;
import dronfox.deliverfresh.customerapplication.customer.TypefaceClass;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Pardeep on 11/8/2015.
 */
public class Fragment_Recharge extends Fragment
{

    TextView simpleText;
    SharedPrefClass sharedPrefClass;
    int amount;
    EditText editText;
    FloatingActionButton view4;
TextView textView;

    TextView textBal;
    MainRetrofit mainRetrofit;
TypefaceClass typefaceClass;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.recharge,container,false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowCustomEnabled(false);


        mainRetrofit=new MainRetrofit();

simpleText=(TextView)view.findViewById(R.id.simpleText);
sharedPrefClass=new SharedPrefClass(getActivity());
textBal=(TextView)view.findViewById(R.id.textBal);
        editText=(EditText)view.findViewById(R.id.edtText);
textView=(TextView)view.findViewById(R.id.currentAmount);
typefaceClass=new TypefaceClass();
        textBal.setTypeface(typefaceClass.functionType(getActivity()));

        simpleText.setTypeface(typefaceClass.functionType(getActivity()));
        textView.setTypeface(typefaceClass.functionType(getActivity()));


        mainRetrofit.functionRetro().userId(sharedPrefClass.getContactNumber(), new Callback<List<PojoClass>>() {
            @Override
            public void success(List<PojoClass> pojoClasses, Response response) {

                for (int i = 0; i < pojoClasses.size(); i++) {
                    textView.setText("" + pojoClasses.get(i).getAcountbalance());
                }

            }

            @Override
            public void failure(RetrofitError retrofitError) {

            }
        });


        view4=(FloatingActionButton)view.findViewById(R.id.view4);
view4.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {

        if(Integer.parseInt(textView.getText().toString()) > 70)
        {
            AlertDialog.Builder builderRc = new AlertDialog.Builder(getActivity());
            builderRc.setTitle("Cannot Recharge");
            builderRc.setMessage("You can only recharge when you account balance is less then 70 rs");
            builderRc.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            builderRc.show();
            builderRc.create();

        }
else if(editText.getText().equals(""))
        {
            editText.setError("Enter Amount");
        }

      else  if (amount>  199) {
            final String dateTime=new SimpleDateFormat("dd-MM-yyyy HH:mm").format(new Date());
           final String timestamp=new SimpleDateFormat("ddMMyyyy_hhMMss").format(new Date());
            final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("Recharge Account");
            builder.setMessage("Amount will reflect in your account once Delivery Boy Got moeny From you");
            builder.setPositiveButton("Recharge", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {


                    final ProgressDialog progressDialog=new ProgressDialog(getActivity());
                    progressDialog.setMessage("Please Wait");
                    progressDialog.show();
                    mainRetrofit.functionRetro().RechargeRequest(sharedPrefClass.getContactNumber(), "" + amount, "Pending", ""+sharedPrefClass.getPlace(),timestamp,""+dateTime, new Callback<String>() {
                        @Override
                        public void success(String s, Response response) {

if(s.equals("sent"))
{
    progressDialog.dismiss();
    editText.setText("");

    AlertDialog.Builder builder1=new AlertDialog.Builder(getActivity());
    builder1.setMessage("Sent");
    builder1.setMessage("Recharge Request is sent to the vendor, Once he will recharge, Amount will be reflect in your account");
    builder1.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {

        }
    });
    builder1.create();
    builder1.show();


}


                        }

                        @Override
                        public void failure(RetrofitError retrofitError) {
                            progressDialog.dismiss();
                        }
                    });



                }
            });
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });

            builder.create();
            builder.show();
        } else {
            Snackbar.make(getActivity().findViewById(android.R.id.content),"Amount Should be greater then 200/-",Snackbar.LENGTH_SHORT).show();

        }
    }
});





        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    amount = Integer.parseInt(s.toString());
                    if (amount < 199) {
                        Snackbar.make(getActivity().findViewById(android.R.id.content), "Amount Should be greater then 200/-", Snackbar.LENGTH_LONG).show();

                    }
                } catch (Exception e) {

                }
            }
        });



        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    getFragmentManager().popBackStack();
                }
                return false;
            }
        });


        return view;
    }
}
