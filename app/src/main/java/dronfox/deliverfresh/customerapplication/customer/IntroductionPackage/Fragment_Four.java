package dronfox.deliverfresh.customerapplication.customer.IntroductionPackage;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;
import java.util.Locale;

import dronfox.deliverfresh.customerapplication.customer.GPSTracker;
import dronfox.deliverfresh.customerapplication.customer.Login;
import dronfox.deliverfresh.customerapplication.customer.NetworkClass;
import dronfox.deliverfresh.customerapplication.customer.R;
import dronfox.deliverfresh.customerapplication.customer.SignUp;
import dronfox.deliverfresh.customerapplication.customer.TypefaceClass;

/**
 * Created by Pardeep on 11/15/2015.
 */
public class Fragment_Four extends Fragment
{
    String getState;


    TypefaceClass typefaceClass;
    TextView readyTitle;
    TextView readDesc;


    NetworkClass networkClass;
    GPSTracker gpsTracker;
    Button btnLogin;
    Button btnSignup;
    LinearLayout linearHide;
FloatingActionButton floatingActionButton;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.intro_four,container,false);
        typefaceClass=new TypefaceClass();
        readyTitle=(TextView)view.findViewById(R.id.readyTitle);
        readDesc=(TextView)view.findViewById(R.id.readyDesc);
        readyTitle.setTypeface(typefaceClass.functionType(getActivity()));

        readDesc.setTypeface(typefaceClass.functionType(getActivity()));

        typefaceClass.setShodow(readyTitle);
        typefaceClass.setShodow(readDesc);


       networkClass=new NetworkClass(getActivity());
        gpsTracker=new GPSTracker(getActivity());



        if(gpsTracker.canGetLocation()==false)
        {

            AlertDialog.Builder builder =new AlertDialog.Builder(getActivity());
            builder.setMessage("Location is turn off, Please Turn On the location");
           builder.setCancelable(false);
            builder.setPositiveButton("Turn On", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent viewIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(viewIntent);
getActivity().finish();
                }
            });

            builder.create();
            builder.show();
        }




        btnLogin=(Button)view.findViewById(R.id.btnLogin);
        btnSignup=(Button)view.findViewById(R.id.btnSignup);
        floatingActionButton=(FloatingActionButton)view.findViewById(R.id.floatButton);
        linearHide=(LinearLayout)view.findViewById(R.id.linearHide);
        linearHide.setVisibility(View.GONE);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Animation animation = AnimationUtils.loadAnimation(getActivity(), R.anim.downtoup);
                linearHide.setAnimation(animation);
                linearHide.setVisibility(View.VISIBLE);

            }
        });


        btnSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SignUp.class);
                startActivity(intent);
                getActivity().finish();
            }
        });


        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent login = new Intent(getActivity(), Login.class);
                startActivity(login);
                getActivity().finish();
            }
        });


        return  view;
    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser == true) {


            Log.e("----",""+gpsTracker.getLatitude()+","+gpsTracker.getLongitude());



            }


        }


        class AsyncTaskLocation extends AsyncTask<Void, Void, Void> {


            ProgressDialog progressDialog;

            @Override
            protected void onPreExecute() {

                progressDialog = new ProgressDialog(getActivity());
                progressDialog.setMessage("Checking if service is available in your Area");
                progressDialog.show();

                super.onPreExecute();
            }

            @Override
            protected Void doInBackground(Void... params) {


                Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
                List<Address> addresses;
                try {


                    addresses = geocoder.getFromLocation(gpsTracker.getLatitude(), gpsTracker.getLongitude(), 1);
                    if (addresses.size() < 1) {

                    } else {
                        getState = addresses.get(0).getLocality();

                    }


                } catch (Exception e) {

                }


                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {


                progressDialog.dismiss();


                if (getState.equalsIgnoreCase("Lucknow") || getState.equals("Banglore")) {

                    //    Snackbar.make(getActivity().findViewById(android.R.id.content), "You Are Available in your city", Snackbar.LENGTH_LONG).show();
                } else {
                    Snackbar.make(getActivity().findViewById(android.R.id.content), "We will be in your city very soon", Snackbar.LENGTH_LONG).show();
                }
                super.onPostExecute(aVoid);
            }
        }


    }

