package dronfox.deliverfresh.customerapplication.customer.DatabaseFolder;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.ArrayList;

import dronfox.deliverfresh.customerapplication.customer.MainActivity;

/**
 * Created by Pardeep on 11/22/2015.
 */
public class DbCounter {
    Context context;
    SQLiteDatabase sqLiteDatabase;
MainActivity mainActivity;
    public DbCounter(Context ctx) {
        context = ctx;
        sqLiteDatabase = context.openOrCreateDatabase("DeliverFresh", context.MODE_PRIVATE, null);
        CreateTable();
    }

    public void CreateTable() {
        String create = "CREATE TABLE IF NOT EXISTS orderDetails(BrandName VARCHAR," +
                "BrandVariety VARCHAR,itemQaunt VARCHAR,itemPrice VARCHAR,totalPrice VARCHAR,numDays VARCHAR," +
                "Category VARCHAR)";
        sqLiteDatabase.execSQL(create);
    }

    public void insertIntoDatabase(String Bname, String Bvariey, String itemQuant, String itemPrice, String total,String numDays,String cat) {

        String select = "SELECT * FROM orderDetails where BrandName='" + Bname + "' AND BrandVariety='" + Bvariey + "'";
        Cursor cursor = sqLiteDatabase.rawQuery(select, null);
        if (cursor.getCount() < 1) {

            String insert = "INSERT INTO orderDetails VALUES('" + Bname + "','" + Bvariey + "','" + itemQuant + "','" + itemPrice + "','" + total + "','"+numDays+"','"+cat+"')";
            sqLiteDatabase.execSQL(insert);
            Toast.makeText(context, "Added To Cart", Toast.LENGTH_LONG).show();
        } else {
            String update = "UPDATE orderDetails SET itemQaunt='" + itemQuant + "',totalPrice='" + total + "' where BrandName='" + Bname + "' AND BrandVariety='" + Bvariey + "'";
            sqLiteDatabase.execSQL(update);
            Toast.makeText(context, "Order Updated", Toast.LENGTH_LONG).show();
        }


    }


    public int  ReturnSize()
    {
        String select="SELECT * FROM orderDetails";
        Cursor cursor=sqLiteDatabase.rawQuery(select,null);
        return cursor.getCount();
    }



    public void SelectValue(ArrayList<String> listBname, ArrayList<String> listVareity,
                            ArrayList<String> listQaunt, ArrayList<String> listPrice, ArrayList<String> cate,LinearLayout emptyCart,LinearLayout filledCart
    ) {

        mainActivity=new MainActivity();
        listBname.clear();
        listVareity.clear();
        listQaunt.clear();
        listPrice.clear();
cate.clear();
        String select = "SELECT * FROM orderDetails";
        Cursor cursor = sqLiteDatabase.rawQuery(select, null);
        if (cursor.getCount() < 1) {

            emptyCart.setVisibility(View.VISIBLE);
            filledCart.setVisibility(View.GONE);
            mainActivity.getInstance().UpdateCartIcon(ReturnSize());
        } else {

            emptyCart.setVisibility(View.GONE);
            filledCart.setVisibility(View.VISIBLE);

            cursor.moveToFirst();
            do {
                String bName = cursor.getString(cursor.getColumnIndex("BrandName"));
                String bVer = cursor.getString(cursor.getColumnIndex("BrandVariety"));
                String itemQuant = cursor.getString(cursor.getColumnIndex("itemQaunt"));
                String total = cursor.getString(cursor.getColumnIndex("totalPrice"));
                String getPrice = cursor.getString(cursor.getColumnIndex("itemPrice"));
                String numdays = cursor.getString(cursor.getColumnIndex("numDays"));
String cat=  cursor.getString(cursor.getColumnIndex("Category"));
                listBname.add("" + bName);
                listVareity.add("" + bVer);
                listQaunt.add("" + itemQuant);
                listPrice.add(getPrice);
                cate.add(""+cat);
                Log.e("---", "" + numdays);

            }
            while (cursor.moveToNext());
        }
    }

    public void Delete() {
        String delete = "DELETE FROM orderDetails";
        sqLiteDatabase.execSQL(delete);
    }




    public String getNumdays()
    {
        String getDays="";
        String select="SELECT numDays FROM orderDetails";
        Cursor cursor=sqLiteDatabase.rawQuery(select,null);
        if(cursor.getCount() < 1)
        {

        }
        else
        {
            cursor.moveToFirst();
            do
            {
            getDays=cursor.getString(cursor.getColumnIndex("numDays"));
            }
            while (cursor.moveToNext());

        }
        return getDays;
    }










    public String TotalSum()
    {
        String text = "";
        String sum = "SELECT SUM(totalPrice*numDays) as GrandPrice FROM orderDetails";
        Cursor cursor = sqLiteDatabase.rawQuery(sum, null);
        if (cursor.getCount() < 1) {

        } else {
            cursor.moveToFirst();
            do {
                text = cursor.getString(cursor.getColumnIndex("GrandPrice"));
                //  textView.setText(" ₹ "+text);
            Log.e("------------",""+text);
            }
            while (cursor.moveToNext());
        }
        return text;
    }


    public void DeleteItem(String BrandName, String Bver) {
        String delete = "DELETE FROM orderDetails WHERE Brandname='" + BrandName + "' and BrandVariety='" + Bver + "'";
        Log.e("-", "" + delete);
        sqLiteDatabase.execSQL(delete);
    }

    public void UpdateDays(String days)
    {
        String upd="UPDATE orderDetails SET numDays='"+days+"'";
        Log.e("00000000",""+upd);
        sqLiteDatabase.execSQL(upd);
    }


}
