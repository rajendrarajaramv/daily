package dronfox.deliverfresh.customerapplication.customer;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import dronfox.deliverfresh.customerapplication.customer.SharedPrefrencesPackage.SharedPrefClass;

/**
 * Created by Pardeep on 11/13/2015.
 */
public class LoginChecker extends AppCompatActivity {


    SharedPrefClass sharedPrefClass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPrefClass = new SharedPrefClass(this);



        if(sharedPrefClass.getLogin().equals("true"))
        {
            Intent intent=new Intent(this,MainActivity.class);
            startActivity(intent);
            finish();
        }
        else if(sharedPrefClass.getLogin().equals("loginscreen"))
        {
            Intent intent=new Intent(this,Login.class);
            startActivity(intent);
            finish();

        } else
        {

            Intent intent=new Intent(this,SplashScreen.class);
            startActivity(intent);
            finish();

        }


    }

}