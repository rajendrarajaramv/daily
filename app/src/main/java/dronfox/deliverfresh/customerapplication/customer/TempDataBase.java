package dronfox.deliverfresh.customerapplication.customer;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;


import java.util.ArrayList;

/**
 * Created by Pardeep on 11/11/2015.
 */
public class TempDataBase
{
    Context ctx;
    SQLiteDatabase sqLiteDatabase;

    TempDataBase(Context context)
    {
        ctx=context;
        sqLiteDatabase=context.openOrCreateDatabase("DeliverFresh",context.MODE_PRIVATE,null);
    CreateTable();
    }


    public void CreateTable()
    {
        String create="CREATE TABLE IF NOT EXISTS DeliverTempTable(city VARCHAR,place VARCHAR,apartment VARCHAR,tower VARCHAR)";
        sqLiteDatabase.execSQL(create);
    }

    public void InsertInTable(String city,String place,String apartment,String tower)
    {
        String insert="INSERT INTO DeliverTempTable VALUES('"+city+"','"+place+"','"+apartment+"','"+tower+"')";
    sqLiteDatabase.execSQL(insert);
    }

    public void Size()
    {
        String sel="SELECT * FROM DeliverTempTable";
        Cursor cursor=sqLiteDatabase.rawQuery(sel,null);
    }



    public void SelectCity(ArrayList<String> strings)
    {

        strings.clear();
String selectCity="SELECT DISTINCT city from DeliverTempTable";
        Cursor cursor=sqLiteDatabase.rawQuery(selectCity,null);
        if(cursor.getCount() < 1)
        {

        }
        else
        {
            cursor.moveToFirst();
            do
            {


                String getCity=cursor.getString(cursor.getColumnIndex("city"));

                    strings.add(""+getCity);
                //city.add(""+city);
            }
            while (cursor.moveToNext());
        }
    }



    public void SelectPlace(String city,ArrayList<String> getPlace)
     {
         getPlace.clear();
        String select="SELECT  DISTINCT place from DeliverTempTable where city='"+city+"'";
        Cursor cursor=sqLiteDatabase.rawQuery(select,null);
        if(cursor.getCount() < 1)
        {

        }
        else
        {
            cursor.moveToFirst();
            do
            {
            String Place=cursor.getString(cursor.getColumnIndex("place"));
                getPlace.add(Place);
            }


            while (cursor.moveToNext());


        }
    }



    public void SelectApartment(String place,ArrayList<String> apartment)
    {

        apartment.clear();
        String select="SELECT  DISTINCT apartment from DeliverTempTable where place='"+place+"'";
        Cursor cursor=sqLiteDatabase.rawQuery(select,null);
        if(cursor.getCount() < 1)
        {

        }
        else
        {
            cursor.moveToFirst();
            do
            {
                String Place=cursor.getString(cursor.getColumnIndex("apartment"));
                apartment.add(Place);
            }
            while (cursor.moveToNext());



        }
    }



    public void SelectFlatNumber(String apartment,ArrayList<String> tower)
    {
        tower.clear();
        String select="SELECT  DISTINCT tower from DeliverTempTable where apartment='"+apartment+"'";
        Cursor cursor=sqLiteDatabase.rawQuery(select,null);
        if(cursor.getCount() < 1)
        {

        }
        else
        {
            cursor.moveToFirst();
            do
            {
                String apart=cursor.getString(cursor.getColumnIndex("tower"));

                tower.add("" + apart);
            }
            while (cursor.moveToNext());


        }
    }
}
