package dronfox.deliverfresh.customerapplication.customer.DatabaseFolder;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

/**
 * Created by Pardeep on 11/22/2015.
 */
public class CityRecords
{
    Context ctx;
    SQLiteDatabase sqLiteDatabase;
    public CityRecords(Context context)
    {
        ctx=context;
        sqLiteDatabase=context.openOrCreateDatabase("DeliverFresh",context.MODE_PRIVATE,null);
        CreateTable();
    }


    public void CreateTable()
    {
        String create="CREATE TABLE IF NOT EXISTS CityRecord(CityName VARCHAR,AreaName VARCHAR)";
        sqLiteDatabase.execSQL(create);
    }

    public void insertDb(String city,String areaName)
    {
        String insert="INSERT INTO CityRecord VALUES('"+city+"','"+areaName+"')";
        sqLiteDatabase.execSQL(insert);
    }


    public void Select(ArrayList<String> city)
    {
        city.clear();
        String select="SELECT DISTINCT CityName from CityRecord";
        Cursor cursor=sqLiteDatabase.rawQuery(select,null);
        if(cursor.getCount() < 1)
        {

        }
        else
        {
            cursor.moveToFirst();
            do
            {
             String getCity=cursor.getString(cursor.getColumnIndex("CityName"));
                city.add(getCity);
            }while (cursor.moveToNext());

        }
    }


    public void Delete()
    {
        String delete="DELETE FROM CityRecord";
        sqLiteDatabase.execSQL(delete);
    }

    public void SeelctArea(ArrayList<String> area,String name)
    {
        area.clear();

        String select="SELECT AreaName from CityRecord where CityName='"+name+"'";
        Cursor cursor=sqLiteDatabase.rawQuery(select,null);
        if(cursor.getCount() < 1)
        {

        }
        else
        {
            cursor.moveToFirst();
            do
            {
                String getCity=cursor.getString(cursor.getColumnIndex("AreaName"));
                area.add(getCity);
            }while (cursor.moveToNext());

        }
    }

}
