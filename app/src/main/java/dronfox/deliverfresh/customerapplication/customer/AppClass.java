package dronfox.deliverfresh.customerapplication.customer;

import android.app.Application;

import com.splunk.mint.Mint;

/**
 * Created by Pardeep on 1/20/2016.
 */
public class AppClass extends Application
{
    @Override
    public void onCreate() {
        super.onCreate();
        Mint.initAndStartSession(AppClass.this, "56804179");
    }
}
