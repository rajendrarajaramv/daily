package dronfox.deliverfresh.customerapplication.customer.IntroductionPackage;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import dronfox.deliverfresh.customerapplication.customer.R;
import dronfox.deliverfresh.customerapplication.customer.TypefaceClass;

/**
 * Created by Pardeep on 11/15/2015.
 */
public class Fragment_Two extends Fragment
{

    TypefaceClass typefaceClass;
    TextView rechargeTitle;
    TextView rechargeDesc;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {



        View view=inflater.inflate(R.layout.intro_two,container,false);

        typefaceClass=new TypefaceClass();
        rechargeTitle=(TextView)view.findViewById(R.id.rechargeTitle);
        rechargeDesc=(TextView)view.findViewById(R.id.rechargeDesc);
        rechargeDesc.setTypeface(typefaceClass.functionType(getActivity()));

        rechargeTitle.setTypeface(typefaceClass.functionType(getActivity()));

        typefaceClass.setShodow(rechargeTitle);
        typefaceClass.setShodow(rechargeDesc);

        return  view;
    }
}
