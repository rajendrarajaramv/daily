package dronfox.deliverfresh.customerapplication.customer;

import java.util.List;

import dronfox.deliverfresh.customerapplication.customer.JsonLocation.PojoClassLocation;
import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Query;

/**
 * Created by Pardeep on 11/11/2015.
 */
public interface RetrofitInterface
{

    @GET("/API/NewApi/FetchLocationAndAppartments.php")
    public void ApartmentData(Callback<List<PojoClass>> callback);

    @FormUrlEncoded
    @POST("/API/NewApi/Registration.php")
    public void InsertRegistration(@Field("name") String name, @Field("contactnumber") String userContact,
                                   @Field("city") String city,
                                   @Field("gcmId") String gcmId,
                                   @Field("password") String password,
                                   @Field("isUserTrue")String isTrue,
                                   @Field("address")String balance,
                                   @Field("accountbalance") String accountbalance,
                                   @Field("placeName") String placeName,


                                   Callback<String> callback);

    @FormUrlEncoded
    @POST("/API/NewApi/CheckLogin.php")
    public void CheckLogin(@Field("contactnumber") String contact, @Field("password") String password,@Field("gcm") String gcmid, Callback<List<PojoClass>> callback);


@FormUrlEncoded
    @POST("/API/NewApi/userValidations.php")
    public  void checkValidation(@Field("contactnumber") String contact,Callback<List<PojoClass>> callback);




@FormUrlEncoded
    @POST("/API/NewApi/MilkAvailable.php")
    public void MilkDetails(@Field("availCity") String city,@Field("availArea") String area,Callback<List<PojoClass>> callback);




    @FormUrlEncoded
    @POST("/API/NewApi/OrderPlace.php")
    public void OrdersPlace(@Field("orderId") String orderId,
                            @Field("orderBy") String orderBy,
                            @Field("orderDate") String orderDate,
                            @Field("orderEnd") String orderEnd,
                            @Field("orderType") String orderType,
                            @Field("orderStatus") String orderStatus,
                            @Field("orderArea") String orderArea,
                            @Field("orderBrandName") String orderBrandName,
                            @Field("orderVariety") String orderVariety,
                            @Field("orderQaunt") String orderQaunt,
                            @Field("isRegular") String isRegular,
                            Callback<String> callback
                            );

@FormUrlEncoded
    @POST("/API/NewApi/SelectAmount.php")
    public void userId(@Field("contactnumber") String contact,Callback<List<PojoClass>> listCallback);





    @FormUrlEncoded
    @POST("/API/NewApi/RechargeRequest.php")
    public void RechargeRequest(@Field("userId") String uId,@Field("Amount") String Amount,@Field("rechargeStatus") String rechargeStatus,@Field("area") String area,
                                @Field("timestamp") String timestamp,   @Field("datetime") String datetime,

                                Callback<String> callb);





    @FormUrlEncoded
    @POST("/API/NewApi/Customer_CheckBalanace.php")
    public void CheckBalance(@Field("contactnumber") String contactnumber,Callback<List<PojoClass>> listCallback);

 //   email=fff@gm.cm, mobile=1234567890, name=fff
//
//    @FormUrlEncoded
//    @POST("/android_otp/request_sms.php")
//    public void sendSms(@Field("email") String mail,@Field("mobile") String phone,@Field("name") String phon,Callback<String> callback);






    @FormUrlEncoded
    @POST("/API/NewApi/customer_GetProfile.php")
    public void getProfile(@Field("contactnumber") String contactnumber,Callback<List<PojoClass>> listCallback);





    @FormUrlEncoded
    @POST("/API/NewApi/showMyProfile.php")
    public  void showMyProfile(@Field("contactnumber") String contact,Callback<List<PojoClass>> listCallback);



    @FormUrlEncoded
    @POST("/API/NewApi/customer_changePassword.php")
    public void updatePassword(@Field("password") String password,@Field("contactnumber") String contactnumber,Callback<String> listCallback);




    @FormUrlEncoded
    @POST("/API/NewApi/customer_DeleteAcount.php")
    public void DeleteAcount(@Field("contactnumber") String contactnumber,Callback<String> stringCallback);



    @FormUrlEncoded
    @POST("/API/NewApi/customer_ShowMyOrder.php")
    public void showMyOrders(@Field("contactnumber") String contactnumber,@Field("dtoday") String dtoday,Callback<List<PojoClass>> listCallback);




    @FormUrlEncoded
    @POST("/API/NewApi/customer_canPost.php")
    public void canIPost(@Field("contactnumber") String contactnumber,@Field("dateToday") String dtoday,Callback<String> listCallback);



    @FormUrlEncoded
    @POST("/API/NewApi/customer_ShowMyOrderHistory.php")
    public void showMyHistory(@Field("contactnumber") String contactnumber,Callback<List<PojoClass>> listCallback);


    @FormUrlEncoded
    @POST("/API/NewApi/customer_DeleteOrders.php")
  public void deleteOrders(@Field("orderId") String orderId,
                           @Field("orderBrandName") String orderBrandName,
                           @Field("orderVariety") String var,
                           @Field("orderBy") String orderBy,
                           @Field("dateToday") String dateToday,
                           @Field("areaAvail") String availArea,
                           Callback<String> callback);


//
//
//    @FormUrlEncoded
//    @POST("/API/NewApi/customer_ExtendMyOrder.php")
//    public void extendMyOrder(@Field("orderId") String orderId,
//                             @Field("orderBrandName") String orderBrandName,@Field("orderVariety") String var,@Field("orderBy") String orderBy,Callback<String> callback);
//




    @FormUrlEncoded
    @POST("/API/NewApi/customer_WantToExtend.php")
    public void wantToExtend(@Field("userId") String uId,
                             @Field("orderDate") String orderDate,
                             @Field("orderEnd") String orderEnd,
                             @Field("orderId") String orderId,


                             Callback<String> callback);




    @FormUrlEncoded
    @POST("/API/NewApi/customer_AdjustMoney.php")
    public void refundMyMoney(@Field("userId") String userId,@Field("orderDate") String orderDates,
                              @Field("orderEnd") String orderEnd,@Field("totalDays") String totalDays,
                              Callback<String> callback);






    @FormUrlEncoded
    @POST("/API/NewApi/customer_CutBalance.php")
    public void cutBalnce(@Field("contactnumber") String contact,@Field("deliverCharges") String deli,Callback<List<PojoClass>> listCallback);




    @POST("/maps/api/geocode/json")
    public void FetchLocationGoogle(@Query("latlng") String latlng,@Query("sensor") String sensor,Callback<PojoClassLocation> pojoClassLocationCallback );


    @POST("/API/NewApi/allPlace.php")
    public void FetchAllPlace(Callback<List<PojoClass>> listCallback) ;






    @FormUrlEncoded
    @POST("/API/NewApi/feedback.php")
    public void feedback(@Field("name") String name,
                             @Field("rate") String rate,
                             @Field("message") String msg,

                         @Field("contactnumber") String contact,


                         Callback<String> callback);




    @FormUrlEncoded
    @POST("/API/NewApi/recievedOrder.php")
    public void semdOrder(@Field("areaVendor") String area,
                          @Field("message") String msg,

                         Callback<String> callback);



}
