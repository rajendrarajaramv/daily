package dronfox.deliverfresh.customerapplication.customer.ViewPagerFragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import dronfox.deliverfresh.customerapplication.customer.BaseAdapterPackage.Baseadapter_History;
import dronfox.deliverfresh.customerapplication.customer.DatabaseFolder.EditOrdersDatabase;
import dronfox.deliverfresh.customerapplication.customer.MainRetrofit;
import dronfox.deliverfresh.customerapplication.customer.PojoClass;
import dronfox.deliverfresh.customerapplication.customer.R;
import dronfox.deliverfresh.customerapplication.customer.SharedPrefrencesPackage.SharedPrefClass;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Pardeep on 11/8/2015.
 */
public class History extends Fragment
{

   MainRetrofit mainRetrofit;
    RecyclerView recyclerView;
    LinearLayoutManager linearLayoutManager;
EditOrdersDatabase editOrdersDatabase;

    ArrayList<String> listOrderId;
    ArrayList<String> listOrderDate;
    ArrayList<String> listOrderEnd;
    ArrayList<String> listOrderBrandName;
    ArrayList<String> listOrderVariety;
    ArrayList<String> listOrderQaunt;
    LinearLayout showLayout;

    LinearLayout noOrder;

    TextView acountBalance;
    SharedPrefClass sharedPrefClass;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


    final View view=inflater.inflate(R.layout.history, container, false);


        mainRetrofit=new MainRetrofit();
        showLayout=(LinearLayout)view.findViewById(R.id.showLayout);

        acountBalance=(TextView)view.findViewById(R.id.acountBalance);
        editOrdersDatabase=new EditOrdersDatabase(getActivity());
        listOrderId=new ArrayList<String>();
        listOrderDate=new ArrayList<String>();
        listOrderEnd=new ArrayList<String>();
        listOrderBrandName=new ArrayList<String>();
        listOrderVariety=new ArrayList<String>();
        listOrderQaunt=new ArrayList<String>();

        noOrder=(LinearLayout)view.findViewById(R.id.noOrder);
        sharedPrefClass=new SharedPrefClass(getActivity());
        linearLayoutManager=new LinearLayoutManager(getActivity());
        recyclerView=(RecyclerView)view.findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(linearLayoutManager);

editOrdersDatabase.delete();

        final ProgressDialog progressDialog=new ProgressDialog(getActivity());
        progressDialog.setMessage("Please Wait");
        progressDialog.show();

        mainRetrofit.functionRetro().showMyHistory(sharedPrefClass.getContactNumber(), new Callback<List<PojoClass>>() {
            @Override
            public void success(List<PojoClass> pojoClasses, Response response) {
                for (int i = 0; i < pojoClasses.size(); i++) {

                    String orderId = pojoClasses.get(i).getOrderId();
                    String balance = pojoClasses.get(i).getAcountbalance();
                    String startDate = pojoClasses.get(i).getOrderDate();
                    String endDate = pojoClasses.get(i).getOrderEnd();
                    String getType = pojoClasses.get(i).getOrderType();
                    String getBrand = pojoClasses.get(i).getOrderBrandName();
                    String getBrandVer = pojoClasses.get(i).getOrderVariety();
                    String getQaunt = pojoClasses.get(i).getOrderQaunt();


                    acountBalance.setText(" ₹ " + balance);
                    editOrdersDatabase.InsertData(orderId, startDate, endDate, getBrand, getBrandVer, getQaunt);

                    //  listOrder.add(getType+"\n\n"+getBrand+"\t \t"+getBrandVer+" \t \t "+getQaunt);
                    try {
                        progressDialog.dismiss();

                    } catch (Exception e) {

                    }

                }


                editOrdersDatabase.SelectQuery(listOrderId, listOrderDate, listOrderEnd, listOrderBrandName, listOrderVariety, listOrderQaunt);
                recyclerView.setAdapter(new Baseadapter_History(getActivity(), listOrderDate, listOrderEnd));


                noOrder.setVisibility(View.GONE);


            }


            @Override
            public void failure(RetrofitError retrofitError) {
                progressDialog.dismiss();
noOrder.setVisibility(View.VISIBLE);
showLayout.setVisibility(View.GONE);
            }
        });

        return view;

    }
}
