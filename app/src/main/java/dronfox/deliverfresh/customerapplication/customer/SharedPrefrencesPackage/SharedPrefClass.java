package dronfox.deliverfresh.customerapplication.customer.SharedPrefrencesPackage;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by Pardeep on 9/24/2015.
 */
public class SharedPrefClass
{


    public String getVendorName() {
      vendorName=GetPrefrenc("vendorName");
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    SetPrefrences("vendorName",vendorName);
    }

    public String getVendorAddress() {
        vendorAddress=GetPrefrenc("vendorAddress");
        return vendorAddress;

    }

    public void setVendorAddress(String vendorAddress) {
        this.vendorAddress = vendorAddress;
    SetPrefrences("vendorAddress",vendorAddress);
    }

    public String getVendorContact() {
        vendorContact=GetPrefrenc("vendorContact");
        return vendorContact;
    }

    public void setVendorContact(String vendorContact) {
        this.vendorContact = vendorContact;
    SetPrefrences("vendorContact",vendorContact);
    }

    String vendorName;
    String vendorAddress;
    String vendorContact;

    Context ctx;

    public String getAddress() {
   address=GetPrefrenc("address");
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
   SetPrefrences("address",address);
    }


    public String getPassword() {
        password=GetPrefrenc("password");
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    SetPrefrences("password",password);
    }

    public String password;

    public String address;

    public String getName() {
    name=GetPrefrenc("name");
        return name;
    }

    public void setName(String name) {
        this.name = name;
    SetPrefrences("name",""+name);
    }

    public String name;


    public String getState() {
        state=GetPrefrenc("city");
        return state;
    }

    public void setState(String state) {
        this.state = state;

        SetPrefrences("city",state);
    }

    public String state;

    public String getPlace() {
       place=GetPrefrenc("place");
        return place;
    }

    public void setPlace(String place) {
        this.place = place;

    SetPrefrences("place",place);
    }

    String place;
    public String getOrderType()
    {
        orderType=GetPrefrenc("orderType");
        return orderType;

    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;

    SetPrefrences("orderType",""+orderType);
    }

    String orderType;


    public int getAccountBalance() {
     accountBalance=GetIntPrefrence("amount");

        return accountBalance;
    }

    public void setAccountBalance(int accountBalance) {
        this.accountBalance = accountBalance;
    SetIntPrefrences("amount",accountBalance);

    }

    int accountBalance;



    public static void SetIntPrefrences(String key, int value) {
        SharedPreferences.Editor edt=preferences.edit();
        edt.putInt(key, value);
        edt.commit();
    }


    public static int GetIntPrefrence(String key)
    {
       return preferences.getInt(key, 0);
    }





    public boolean isShowPopup() {
    showPopup=getBoolPref("popup");
        return showPopup;
    }

    public void setShowPopup(boolean showPopup) {
        this.showPopup = showPopup;
    setBoolPrefrences("popup",showPopup);
    }

    boolean showPopup;


    String login;

    public String getContactNumber() {
        contactNumber=GetPrefrenc("contact");

        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {

        this.contactNumber = contactNumber;
        SetPrefrences("contact",contactNumber);
    }

    String contactNumber;



    public boolean isUservalid() {
       uservalid=getBoolPref("validuser");

        return uservalid;
    }

    public void setUservalid(boolean uservalid) {
        this.uservalid = uservalid;
        setBoolPrefrences("validuser",uservalid);

    }

    boolean uservalid;

    public String getLogin() {
        login=GetPrefrenc("loginCheck");

        return login;
    }

    public void setLogin(String login) {
      SetPrefrences("loginCheck",login);
        this.login = login;
    }




    static SharedPreferences preferences;
    public SharedPrefClass(Context ct)
    {
        ctx=ct;
        preferences= PreferenceManager.getDefaultSharedPreferences(ctx);

    }


    public static void SetPrefrences(String key, String value) {
        SharedPreferences.Editor edt=preferences.edit();
        edt.putString(key, value);
        edt.commit();
    }


    public static String  GetPrefrenc(String key)
    {
        return  preferences.getString(key, "none");
    }







    public static  void setBoolPrefrences(String key,boolean value)
    {
        SharedPreferences.Editor edt=preferences.edit();
        edt.putBoolean(key, value);
        edt.commit();

    }

    public static boolean getBoolPref(String key)
    {
        return preferences.getBoolean(key,false);
    }


}

