package dronfox.deliverfresh.customerapplication.customer;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import dronfox.deliverfresh.customerapplication.customer.DatabaseFolder.DbCounter;
import dronfox.deliverfresh.customerapplication.customer.Navigation_Fragments.Fragement_Home;
import dronfox.deliverfresh.customerapplication.customer.Navigation_Fragments.Fragment_Account;
import dronfox.deliverfresh.customerapplication.customer.Navigation_Fragments.Fragment_Holyday;
import dronfox.deliverfresh.customerapplication.customer.Navigation_Fragments.Fragment_Order;
import dronfox.deliverfresh.customerapplication.customer.Navigation_Fragments.Fragment_Recharge;
import dronfox.deliverfresh.customerapplication.customer.SharedPrefrencesPackage.SharedPrefClass;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Pardeep on 8/28/2015.
 */
public class MainActivity extends ActionBarActivity //implements AdapterView.OnItemClickListener { ActionBarDrawerToggle actionBarDrawerToggle;

{
ShowCaseClass showCaseClass;
    String[] array;


    TextView textMark;
    ImageView cart;
    int clickcount=0;


    ArrayList<String> venderContact;
    ArrayList<String> venderAdress;
    ArrayList<String> vendername;

    public static MainActivity mainActivity;
    public static MainActivity getInstance()
    {

        return mainActivity;
    }
    View bedgeView;

    DbCounter dbCounter;
   //public static  TextView headerVerify;

    MainRetrofit mainRetrofit;

SharedPrefClass sharedPrefClass;
    DrawerLayout drawerLayout;
    ActionBarDrawerToggle actionBarDrawerToggle;
    NavigationView navigationView;
    FragmentTransaction fragmentTransaction;

    TypefaceClass typefaceClass;
    TextView headerVerify;
    TextView headerName;
    TextView headerAmount;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
mainActivity=this;
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
sharedPrefClass=new SharedPrefClass(this);


        venderContact=new ArrayList<String>();
        venderAdress=new ArrayList<String>();;
        vendername=new ArrayList<String>();;


        dbCounter=new DbCounter(this);
        typefaceClass=new TypefaceClass();
      mainRetrofit=new MainRetrofit();

showCaseClass=new ShowCaseClass(this);


UpdateHeader();



        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView=(NavigationView)findViewById(R.id.navigation);

        View view=navigationView.inflateHeaderView(R.layout.headerlayout);

        headerName =(TextView)view.findViewById(R.id.headerName);
     headerName.setText(""+sharedPrefClass.getName());

        headerVerify  =(TextView)view.findViewById(R.id.headerVerify);
        headerAmount=(TextView)view.findViewById(R.id.headerPrice);

        headerAmount.setTypeface(typefaceClass.functionType(this));

        headerName.setTypeface(typefaceClass.functionType(this));
        headerVerify.setTypeface(typefaceClass.functionType(this));
        typefaceClass.setShodow(headerAmount);
        typefaceClass.setShodow(headerName);
        typefaceClass.setShodow(headerVerify);




        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout,
                R.string.drawer_open, R.string.drawer_close) {

            public void onDrawerOpened(View drawerView) {
            }

            public void onDrawerClosed(View view) {
            }
        };

        actionBarDrawerToggle.setDrawerIndicatorEnabled(true);
        drawerLayout.setDrawerListener(actionBarDrawerToggle);



        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {

                fragmentTransaction=getSupportFragmentManager().beginTransaction();

                clickcount=0;
                switch (item.getItemId())
                {

case R.id.action_feedback:

    LayoutInflater layoutInflater=(LayoutInflater)getSystemService(LAYOUT_INFLATER_SERVICE);
    View viewFeedback=layoutInflater.inflate(R.layout.feedback,null);
    AlertDialog.Builder builder=new AlertDialog.Builder(MainActivity.this);


    builder.setTitle("How Would you like to rate us");

   builder.setView(viewFeedback);

    final RatingBar ratingBar=(RatingBar)viewFeedback.findViewById(R.id.Rating);
    final EditText editText=(EditText)viewFeedback.findViewById(R.id.entermsg);


  builder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {
mainRetrofit.functionRetro().feedback(sharedPrefClass.getName(), "" + ratingBar.getRating(), "" + editText.getText().toString(), "" + sharedPrefClass.getContactNumber(), new Callback<String>() {
    @Override
    public void success(String s, Response response) {

    }

    @Override
    public void failure(RetrofitError retrofitError) {

    }
});
      }
  });
    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {

        }
    });

    builder.create();
    builder.show();










    break;

                    case R.id.action_holiday:
                        fragmentTransaction.replace(R.id.content_frame,new Fragment_Holyday());
                        break;
                    case R.id.action_acount:
                        fragmentTransaction.replace(R.id.content_frame,new Fragment_Account());

                        break;

                    case R.id.action_orderList:
                        fragmentTransaction.replace(R.id.content_frame,new Fragment_Order());

                        break;
                    case R.id.action_home:
                        fragmentTransaction.replace(R.id.content_frame,new Fragement_Home());

                        break;
                    case R.id.action_recharge:
                        fragmentTransaction.replace(R.id.content_frame,new Fragment_Recharge());
                        break;

                    case R.id.action_SignOut:

                        sharedPrefClass.setLogin("loginscreen");
                        Intent intentLogin=new Intent(MainActivity.this,Login.class);
                        startActivity(intentLogin);
                        finish();

                        break;


                }
                fragmentTransaction.commit();
                drawerLayout.closeDrawers();
                return  false;
            }
        });


    fragmentTransaction=getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.content_frame,new Fragement_Home());
        fragmentTransaction.commit();

    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK) {

            clickcount++;

           if(clickcount==1)
           {
               fragmentTransaction=getSupportFragmentManager().beginTransaction();
               fragmentTransaction.replace(R.id.content_frame,new Fragement_Home());
               fragmentTransaction.commit();
Toast.makeText(MainActivity.this,"Press Back key again to exist",Toast.LENGTH_LONG).show();
           }
            else
           {
               finish();
           }


            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId())
//        {
//            case R.id.cart:
//
//                break;
//        }


        if (actionBarDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }




        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.actoncart, menu);
        bedgeView= (View) menu.findItem(R.id.cart).getActionView();
        UpdateCartIcon(dbCounter.ReturnSize());
        cart=(ImageView)bedgeView.findViewById(R.id.pic);
        showCaseClass.ShowCase(cart, MainActivity.this, "Close", "You can check your cart here", "0");
        cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentTransaction=getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.content_frame,new ProceedPayment());
                fragmentTransaction.commit();
            }
        });


        View viewVendor=(View)menu.findItem(R.id.vendor).getActionView();

        ImageView imageView=(ImageView)viewVendor.findViewById(R.id.vendorim);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent ShowVendors=new Intent(MainActivity.this,ShowVendors.class);
                startActivity(ShowVendors);


                }
        });




        return super.onCreateOptionsMenu(menu);

    }

    @Override
    public void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        Log.e("post create", "working");
        actionBarDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Log.e("confi change","working");
        actionBarDrawerToggle.onConfigurationChanged(newConfig);
    }


    @Override
    protected void onRestart() {
        super.onRestart();

        // objLocalDb.SelectPersonalInfo(listName, listExper, listAvail, listprofilepic, listUserid);
//        Bitmap bm= BitmapFactory.decodeFile(listprofilepic.get(0));
//
//
//
//        circularImageView.setImageBitmap(bm);
//        textView.setText("" + listName.get(0));



    }




    public void UpdateHeader()
    {

       // Toast.makeText(MainActivity.this,"Update Header"+sharedPrefClass.getContactNumber(),Toast.LENGTH_LONG).show();
        mainRetrofit.functionRetro().getProfile(sharedPrefClass.getContactNumber(), new Callback<List<PojoClass>>() {
            @Override
            public void success(List<PojoClass> pojoClasses, Response response) {
                for (int i = 0; i < pojoClasses.size(); i++) {
                    String amount = pojoClasses.get(i).getAcountbalance();
//    String name=pojoClasses.get(i).getName();
                    String isUserTrue = pojoClasses.get(i).getIsUserTrue();
                    Log.e("isUserTru", "" + amount);

//
//    headerName.setText(""+name);
                    if (isUserTrue.equals("true")) {
                        headerVerify.setText("verified");
                        sharedPrefClass.setUservalid(true);
                    } else {
                        headerVerify.setText("" + isUserTrue);
                        sharedPrefClass.setUservalid(false);
                    }
                    headerAmount.setText("₹ " + amount);
//
                    sharedPrefClass.setAccountBalance(Integer.parseInt(amount));



                }
            }

            @Override
            public void failure(RetrofitError retrofitError) {

            }
        });

    }




    public void UpdateCartIcon(int size)
    {
        textMark=(TextView)bedgeView.findViewById(R.id.counter);

        if(size < 1)
        {
            textMark.setVisibility(View.GONE);
        }
        else {
            textMark.setVisibility(View.VISIBLE);
        }
    }
}
