package dronfox.deliverfresh.customerapplication.customer;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.util.List;

import dronfox.deliverfresh.customerapplication.customer.SharedPrefrencesPackage.SharedPrefClass;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Pardeep on 11/7/2015.
 */
public class Login extends AppCompatActivity implements View.OnClickListener
{
    Button button;
    EditText loginContact;
    EditText loginPassword;
TextView forgot;
    TextView needAccount;

    SharedPrefClass sharedPrefClass;
GoogleCloudMessaging googleCloudMessaging;
    public static String getGcmId;
    public static String projectNumber="60095088152";
MainRetrofit mainRetrofit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    setContentView(R.layout.loginscreen);
        googleCloudMessaging = GoogleCloudMessaging.getInstance(this);
        TextView textLogin=(TextView)findViewById(R.id.tagLogin);

        TextView textDesc=(TextView)findViewById(R.id.tagDesc);

mainRetrofit=new MainRetrofit();
        TypefaceClass typefaceClass=new TypefaceClass();
        textLogin.setTypeface(typefaceClass.functionType(this));

        textDesc.setTypeface(typefaceClass.functionType(this));
sharedPrefClass=new SharedPrefClass(this);

        needAccount=(TextView)findViewById(R.id.needAccount);
        needAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(Login.this,SignUp.class);
                startActivity(intent);
                finish();
            }
        });



        loginContact=(EditText)findViewById(R.id.login_Number);
        loginPassword=(EditText)findViewById(R.id.loginPassword);

        new AsyncTaskGcm().execute();

        forgot=(TextView)findViewById(R.id.forgotpasswrd);
        forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(Login.this,ForgotPassword.class);
                startActivity(intent);
            }
        });
        button=(Button)findViewById(R.id.button);
        button.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button:
                if (loginContact.getText().toString().length() < 10 || loginContact.getText().toString().equals(""))

                {
                    loginContact.setError("Contact Number Should be atleast 10 digits");
                } else if (loginPassword.getText().toString().equals("")) {
                    loginPassword.setError("Enter Password");
                } else

                {


                    final ProgressDialog progressDialog=new ProgressDialog(Login.this);
                    progressDialog.setMessage("Loging into Account ....");
                    progressDialog.show();
                    mainRetrofit.functionRetro().CheckLogin(loginContact.getText().toString(), loginPassword.getText().toString(), getGcmId, new Callback<List<PojoClass>>() {
                        @Override
                        public void success(List<PojoClass> pojoClasses, Response response) {
                            for (int i = 0; i < pojoClasses.size(); i++) {
                                sharedPrefClass.setPlace(pojoClasses.get(i).getPlaceName());
                                sharedPrefClass.setState("" + pojoClasses.get(i).getCity());
                                sharedPrefClass.setContactNumber(""+pojoClasses.get(i).getContactnumber());

                                sharedPrefClass.setName(""+pojoClasses.get(i).getName());



                            }

                            sharedPrefClass.setLogin("true");
                           // sharedPrefClass.setPlace(sharedPrefClass.getPlace());
                            //sharedPrefClass.
                            sharedPrefClass.setContactNumber(loginContact.getText().toString());

                            Intent intent = new Intent(Login.this, MainActivity.class);
                            startActivity(intent);
                           finish();
progressDialog.dismiss();

                        }

                        @Override
                        public void failure(RetrofitError retrofitError) {

                            progressDialog.dismiss();
         DisplayDialog("Wrong Credentials,Please Try Again");

                        }
                    });
                }
        }
    }

    public void DisplayDialog(String s)
    {
        AlertDialog.Builder builder=new AlertDialog.Builder(this);
        builder.setMessage(""+s);
        builder.setTitle("Wrong Username/Password");
        builder.setPositiveButton("Try Again", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.create();
        builder.show();
    }



    public class AsyncTaskGcm extends AsyncTask<Void,Void,Void>
    {

        @Override
        protected Void doInBackground(Void... params) {
            try {
                getGcmId = googleCloudMessaging.register(projectNumber);
            }catch (Exception e)
            {

            }
                return null;
        }
   }

}
