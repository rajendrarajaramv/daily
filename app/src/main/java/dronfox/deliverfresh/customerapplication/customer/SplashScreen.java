package dronfox.deliverfresh.customerapplication.customer;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import dronfox.deliverfresh.customerapplication.customer.IntroductionPackage.Main_IntroductionClass;

/**
 * Created by Pardeep on 11/7/2015.
 */
public class SplashScreen extends AppCompatActivity
{
    RelativeLayout linearLayout;
    ImageView mainImage;
TypefaceClass typeface;
    TextView title;
    TextView tagLine;


    CountDownTimer countDownTimer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splashscreen);
        title = (TextView) findViewById(R.id.title);
        tagLine = (TextView) findViewById(R.id.tagLine);
        mainImage = (ImageView) findViewById(R.id.mainImage);
        Animation animation= AnimationUtils.loadAnimation(this,R.anim.elasticanimtion);
        mainImage.setAnimation(animation);

        typeface=new TypefaceClass();

   //     typeface = Typeface.createFromAsset(getAssets(), "roboto.ttf");

        title.setTypeface(typeface.functionType(this));
        title.setShadowLayer(2.0f, 2.0f, 2.0f, Color.BLACK);


        tagLine.setTypeface(typeface.functionType(this));

        title.setShadowLayer(2.0f, 2.0f, 2.0f, Color.BLACK);


        linearLayout = (RelativeLayout) findViewById(R.id.linearLayout);

linearLayout.getBackground().setAlpha(156);
    countDownTimer=new CountDownTimer(3000,3000) {
        @Override
        public void onTick(long millisUntilFinished) {

        }

        @Override
        public void onFinish() {
            Intent intent=new Intent(SplashScreen.this, Main_IntroductionClass.class);
            startActivity(intent);
            finish();
        }
    }.start();

    }
}
