package dronfox.deliverfresh.customerapplication.customer.BaseAdapterPackage;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import dronfox.deliverfresh.customerapplication.customer.R;
import dronfox.deliverfresh.customerapplication.customer.TypefaceClass;

/**
 * Created by Pardeep on 10/20/2015.
 */
public class Baseadapter_HistoryOrders extends RecyclerView.Adapter<Baseadapter_HistoryOrders.ViewHolder>
{
    Context context;
    ArrayList<String> listOrderBrandName;
    ArrayList<String> listOrderVariety;
    ArrayList<String> listOrderQaunt;
TypefaceClass typefaceClass;
    public Baseadapter_HistoryOrders(Context ctx, ArrayList<String> OrderBrandName,
                                     ArrayList<String> OrderVariety,
                                     ArrayList<String> OrderQaunt)
    {
        context=ctx;

        listOrderBrandName=OrderBrandName;
        listOrderVariety=OrderVariety;
        listOrderQaunt=OrderQaunt;
typefaceClass=new TypefaceClass();
    }
    @Override
    public Baseadapter_HistoryOrders.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View inflaterview= LayoutInflater.from(context).inflate(R.layout.baseadapter_dialogbottom, parent, false);
        ViewHolder vh=new ViewHolder(inflaterview);


        return vh;
    }

    @Override
    public void onBindViewHolder(Baseadapter_HistoryOrders.ViewHolder holder, final int position) {
holder.itemBrand.setText(""+listOrderBrandName.get(position));
        holder.bottom_ItemVer.setText(""+listOrderVariety.get(position));
        holder.itemQaunt.setText(""+listOrderQaunt.get(position));
    holder.itemBrand.setTypeface(typefaceClass.functionType(context));

        holder.bottom_ItemVer.setTypeface(typefaceClass.functionType(context));

        holder.itemQaunt.setTypeface(typefaceClass.functionType(context));

    }

    @Override
    public int getItemCount() {
        return listOrderBrandName.size();
    }


    public class ViewHolder extends  RecyclerView.ViewHolder
    {
TextView bottom_ItemVer;
        TextView itemBrand;
        TextView itemQaunt;
        public ViewHolder(View itemView) {
            super(itemView);

            bottom_ItemVer=(TextView)itemView.findViewById(R.id.bottom_ItemVer);

            itemBrand=(TextView)itemView.findViewById(R.id.itemBrand);

            itemQaunt=(TextView)itemView.findViewById(R.id.bottom_itemQaunt);
        }
    }
}
