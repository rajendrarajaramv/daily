package dronfox.deliverfresh.customerapplication.customer.ViewPagerFragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import dronfox.deliverfresh.customerapplication.customer.BaseAdapterPackage.Baseadapter_UpcomingOrders;
import dronfox.deliverfresh.customerapplication.customer.DatabaseFolder.EditOrdersDatabase;
import dronfox.deliverfresh.customerapplication.customer.R;

/**
 * Created by Pardeep on 11/8/2015.
 */
public class UpcomingOrders extends Fragment
{
RecyclerView recyclerView;
    ArrayList<String> listOrderId;
    ArrayList<String> listOrderBrandName;
    ArrayList<String> listOrderBrandVariety;
    ArrayList<String> listQaunt;
    String getDate;

    LinearLayout upcoming_HideLayout;

    LinearLayoutManager linearLayoutManager;
EditOrdersDatabase editOrdersDatabase;
int getHour;
    String getNextDate;
    TextView tagLines;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        View view=inflater.inflate(R.layout.baseadapter_upcoming, container, false);


        upcoming_HideLayout=(LinearLayout)view.findViewById(R.id.upcomingHideLayout);


        tagLines=(TextView)view.findViewById(R.id.tagLines);
        recyclerView=(RecyclerView)view.findViewById(R.id.recy);
        linearLayoutManager=new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);

        listOrderId=new ArrayList<String>();
       listOrderBrandName=new ArrayList<String>();;
       listOrderBrandVariety=new ArrayList<String>();;
       listQaunt=new ArrayList<String>();



getHour=Integer.parseInt("" + new SimpleDateFormat("HH").format(new Date()));

editOrdersDatabase=new EditOrdersDatabase(getActivity());



        Calendar calendar = Calendar.getInstance();
        Date today = calendar.getTime();
        calendar.add(Calendar.DAY_OF_YEAR, 1);
        Date tomorrow = calendar.getTime();

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");


       getDate = dateFormat.format(tomorrow);

tagLines.setText("No Order Found for "+getDate);


        return view;

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);


    if(isVisibleToUser==true)
    {
        editOrdersDatabase.FetchUpcomingOrders(getDate,listOrderId,listOrderBrandName,listOrderBrandVariety,listQaunt,recyclerView,upcoming_HideLayout);
        recyclerView.setAdapter(new Baseadapter_UpcomingOrders(getActivity(),listOrderId,listOrderBrandName,listOrderBrandVariety,listQaunt));

    }
    }
}
