package dronfox.deliverfresh.customerapplication.customer.Navigation_Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import dronfox.deliverfresh.customerapplication.customer.BaseAdapterPackage.Baseadapter_Home;
import dronfox.deliverfresh.customerapplication.customer.DatabaseFolder.DatabaseMilkType;
import dronfox.deliverfresh.customerapplication.customer.MainRetrofit;
import dronfox.deliverfresh.customerapplication.customer.R;
import dronfox.deliverfresh.customerapplication.customer.ServiceFetchItems;
import dronfox.deliverfresh.customerapplication.customer.SharedPrefrencesPackage.SharedPrefClass;
import dronfox.deliverfresh.customerapplication.customer.ShowCaseClass;
import dronfox.deliverfresh.customerapplication.customer.TypefaceClass;

/**
 * Created by Pardeep on 11/7/2015.
 */
public class Fragement_Home extends Fragment {

   public static ArrayList<String> listName;
    public static ArrayList<String> listImage;
    public static Fragement_Home  mainActivity;
    public static Fragement_Home getInstance() {
   return mainActivity;
    }


    LinearLayout hideLayout;
    RecyclerView recyclerView;

    GridLayoutManager linearLayoutManager;
    LinearLayout linearLayout;
   DatabaseMilkType databaseMilkType;
ShowCaseClass showCaseClass;

TextView textTagLine;
SharedPrefClass sharedPrefClass;
    MainRetrofit mainRetrofit;
TypefaceClass typefaceClass;

    TextView textNoAvail;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {



        View view = inflater.inflate(R.layout.recyclerviewxml, container, false);
       sharedPrefClass=new SharedPrefClass(getActivity());


        mainActivity=this;

      //  Log.e("here value",""+sharedPrefClass.getState()+"----"+sharedPrefClass.getPlace());

                ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowCustomEnabled(false);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Deliver Fresh");

       // setRetainInstance(true);
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerview);
       typefaceClass=new TypefaceClass();
        listName=new ArrayList<String>();
listImage=new ArrayList<String>();
mainRetrofit=new MainRetrofit();
        textNoAvail=(TextView)view.findViewById(R.id.textNoAvail);

        textNoAvail.setTypeface(typefaceClass.functionType(getActivity()));
showCaseClass=new ShowCaseClass(getActivity());



        hideLayout=(LinearLayout)view.findViewById(R.id.hideLayout);
textTagLine=(TextView)view.findViewById(R.id.textTagLine);
textTagLine.setTypeface(typefaceClass.functionType(getActivity()));
        //setHasOptionsMenu(true);
        databaseMilkType=new DatabaseMilkType(getActivity());

        linearLayoutManager = new GridLayoutManager(getActivity(),3);
        recyclerView.setLayoutManager(linearLayoutManager);


if(databaseMilkType.getSize()< 1) {
    hideLayout.setVisibility(View.VISIBLE);
    textTagLine.setText("So far we are not available in "+sharedPrefClass.getPlace()+" Once we are available in your area we will notify you");

    getActivity().startService(new Intent(getActivity(),ServiceFetchItems.class));
}
        else {
  UpdateList();

}

        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    getFragmentManager().popBackStackImmediate();
                }
                return false;
            }
        });



        return view;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

       menu.clear();
        super.onCreateOptionsMenu(menu, inflater);
    }





public void UpdateList() {

    databaseMilkType.ShowAllCategory(listName, listImage);
    recyclerView.setAdapter(new Baseadapter_Home(getActivity(), listName, listImage));

    hideLayout.setVisibility(View.GONE);


}

}
