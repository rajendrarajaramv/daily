package dronfox.deliverfresh.customerapplication.customer;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

import dronfox.deliverfresh.customerapplication.customer.BaseAdapterPackage.Baseadapter_ShowVendors;
import dronfox.deliverfresh.customerapplication.customer.SharedPrefrencesPackage.SharedPrefClass;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Pardeep on 6/27/2016.
 */
public class ShowVendors extends AppCompatActivity
{
    Toolbar toolbar;
    MainRetrofit mainRetrofit;
SharedPrefClass sharedPrefClass;
    RecyclerView recyclerviewVendrs;
    LinearLayoutManager linearLayoutManager;
    ArrayList<String> getAddress;
    ArrayList<String> getName;
    ArrayList<String> getPhone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.showvendors);
        toolbar=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getAddress=new ArrayList<String>();
        getName=new ArrayList<String>();;
        getPhone=new ArrayList<String>();;
mainRetrofit=new MainRetrofit();
        sharedPrefClass=new SharedPrefClass(this);
        recyclerviewVendrs=(RecyclerView)findViewById(R.id.recyclerviewVendrs);
        linearLayoutManager=new LinearLayoutManager(this);
        recyclerviewVendrs.setLayoutManager(linearLayoutManager);
        final ProgressDialog progressDialog=new ProgressDialog(this);
        progressDialog.setMessage("Please Wait");
        progressDialog.show();
        mainRetrofit.functionRetro().showMyProfile(sharedPrefClass.getPlace(), new Callback<List<PojoClass>>() {
            @Override
            public void success(List<PojoClass> pojoClasses, Response response) {
                for (int i = 0; i < pojoClasses.size(); i++) {


                    getPhone.add(pojoClasses.get(i).getVendorContact());
                    getAddress.add(pojoClasses.get(i).getAdress());
                    getName.add(pojoClasses.get(i).getVendorName());


                }
            recyclerviewVendrs.setAdapter(new Baseadapter_ShowVendors(ShowVendors.this,getName,getAddress,getPhone));
            progressDialog.dismiss();;
            }

            @Override
            public void failure(RetrofitError retrofitError) {

            }
        });




    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
               finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
