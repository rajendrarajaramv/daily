package dronfox.deliverfresh.customerapplication.customer;

/**
 * Created by Pardeep on 11/11/2015.
 */
public class PojoClass
{
    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    String adress;
    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getVendorContact() {
        return vendorContact;
    }

    public void setVendorContact(String vendorContact) {
        this.vendorContact = vendorContact;
    }

    String vendorName;
    String vendorContact;

    public String getCatPic() {
        return catPic;
    }

    public void setCatPic(String catPic) {
        this.catPic = catPic;
    }

    String catPic;
    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    String catName;

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String cityName;


    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    String address;
    public String getAcountbalance() {
        return acountbalance;
    }

    public void setAcountbalance(String acountbalance) {
        this.acountbalance = acountbalance;
    }

    String acountbalance;

    public String getPlaceName() {
        return placeName;
    }

    public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }

    String placeName;
    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    String imagePath;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    String orderId;
    public String getOrderVariety() {
        return orderVariety;
    }

    public void setOrderVariety(String orderVariety) {
        this.orderVariety = orderVariety;
    }

    String orderVariety;

    public String getOrderBrandName() {
        return orderBrandName;
    }

    public void setOrderBrandName(String orderBrandName) {
        this.orderBrandName = orderBrandName;
    }

    String orderBrandName;



    String category;
    String Brandname;
    String BrandVariety;
    String BrandPrice;
    String availCity;

    public String getOrderQaunt() {
        return orderQaunt;
    }

    public void setOrderQaunt(String orderQaunt) {
        this.orderQaunt = orderQaunt;
    }

    String orderQaunt;


    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    String orderType;

    public String getIsRegular() {
        return isRegular;
    }

    public void setIsRegular(String isRegular) {
        this.isRegular = isRegular;
    }

    String isRegular;

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getOrderEnd() {
        return orderEnd;
    }

    public void setOrderEnd(String orderEnd) {
        this.orderEnd = orderEnd;
    }

    String orderDate;
    String orderEnd;





    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    String password;

    public String getFlat() {
        return flat;
    }

    public void setFlat(String flat) {
        this.flat = flat;
    }

    String flat;

    public String getTower() {
        return tower;
    }

    public void setTower(String tower) {
        this.tower = tower;
    }

    String tower;


    public String getApartment() {
        return apartment;
    }

    public void setApartment(String apartment) {
        this.apartment = apartment;
    }

    String apartment;


    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    String place;


    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    String city;


    public String getContactnumber() {
        return contactnumber;
    }

    public void setContactnumber(String contactnumber) {
        this.contactnumber = contactnumber;
    }

    String contactnumber;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    String name;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getBrandname() {
        return Brandname;
    }

    public void setBrandname(String brandname) {
        Brandname = brandname;
    }

    public String getBrandVariety() {
        return BrandVariety;
    }

    public void setBrandVariety(String brandVariety) {
        BrandVariety = brandVariety;
    }

    public String getBrandPrice() {
        return BrandPrice;
    }

    public void setBrandPrice(String brandPrice) {
        BrandPrice = brandPrice;
    }

    public String getAvailCity() {
        return availCity;
    }

    public void setAvailCity(String availCity) {
        this.availCity = availCity;
    }

    public String getAvailArea() {
        return availArea;
    }

    public void setAvailArea(String availArea) {
        this.availArea = availArea;
    }

    String availArea;







    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    String datetime;

    public String getAcountBalance() {
        return acountBalance;
    }

    public void setAcountBalance(String acountBalance) {
        this.acountBalance = acountBalance;
    }

    String acountBalance;


    String MilkName;

    public String getMilkName() {
        return MilkName;
    }

    public void setMilkName(String milkName) {
        MilkName = milkName;
    }

    public String getFullCreamPrice() {
        return fullCreamPrice;
    }

    public void setFullCreamPrice(String fullCreamPrice) {
        this.fullCreamPrice = fullCreamPrice;
    }

    public String getTonedMilkPrice() {
        return tonedMilkPrice;
    }

    public void setTonedMilkPrice(String tonedMilkPrice) {
        this.tonedMilkPrice = tonedMilkPrice;
    }

    public String getButterMilkPrice() {
        return butterMilkPrice;
    }

    public void setButterMilkPrice(String butterMilkPrice) {
        this.butterMilkPrice = butterMilkPrice;
    }

    public String getMilkAvailCity() {
        return milkAvailCity;
    }

    public void setMilkAvailCity(String milkAvailCity) {
        this.milkAvailCity = milkAvailCity;
    }

    String fullCreamPrice;
    String tonedMilkPrice;
    String butterMilkPrice;
    String milkAvailCity;






















    String city_name;

    public String getIsUserTrue() {
        return isUserTrue;
    }

    public void setIsUserTrue(String isUserTrue) {
        this.isUserTrue = isUserTrue;
    }

    String isUserTrue;

    public String getCity_name() {
        return city_name;
    }

    public void setCity_name(String city_name) {
        this.city_name = city_name;
    }

    public String getPlace_name() {
        return place_name;
    }

    public void setPlace_name(String place_name) {
        this.place_name = place_name;
    }

    public String getApartment_name() {
        return apartment_name;
    }

    public void setApartment_name(String apartment_name) {
        this.apartment_name = apartment_name;
    }

    public String getTower_Name() {
        return Tower_Name;
    }

    public void setTower_Name(String tower_Name) {
        Tower_Name = tower_Name;
    }

    String place_name;
    String apartment_name;
    String Tower_Name;

}
