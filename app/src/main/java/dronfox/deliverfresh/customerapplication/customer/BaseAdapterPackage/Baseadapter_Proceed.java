package dronfox.deliverfresh.customerapplication.customer.BaseAdapterPackage;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import dronfox.deliverfresh.customerapplication.customer.DatabaseFolder.DbCounter;
import dronfox.deliverfresh.customerapplication.customer.R;
import dronfox.deliverfresh.customerapplication.customer.SharedPrefrencesPackage.SharedPrefClass;
import dronfox.deliverfresh.customerapplication.customer.TypefaceClass;

/**
 * Created by Pardeep on 10/20/2015.
 */
public class Baseadapter_Proceed extends RecyclerView.Adapter<Baseadapter_Proceed.ViewHolder>
{
    Context context;

    ArrayList<String> listBname;
    ArrayList<String> listVareity;
    ArrayList<String> listQaunt;



    ArrayList<String> listPrice;
    ArrayList<String> listCat;
    ArrayList<String> grandTotal;
int countNum=0;
    int getPrice;
    DbCounter dbCounter;
TextView textView;
    int finalPrice;
    SharedPrefClass sharedPrefClass;
    int total;
    boolean bit=false;
    LinearLayout  layoutEmptyCart;
    LinearLayout  layoutFilledCart;

    TypefaceClass typefaceClass;

    public Baseadapter_Proceed(Context ctx, ArrayList<String> Bname,
                               ArrayList<String> Vareity, ArrayList<String> Qaunt,
                               ArrayList<String> Total, TextView textView1,
                               ArrayList<String> lPrice, ArrayList<String> cat,
                               LinearLayout filledCart,LinearLayout emptyCart)


    {
        context=ctx;
        listBname=Bname;
        listVareity=Vareity;
        listQaunt=Qaunt;
        grandTotal=Total;
        listPrice=lPrice;
textView=textView1;

        typefaceClass = new TypefaceClass();

        layoutEmptyCart=emptyCart;
        layoutFilledCart=filledCart;

        listCat=cat;
        dbCounter=new DbCounter(context);
sharedPrefClass=new SharedPrefClass(context);


    }
    @Override
    public Baseadapter_Proceed.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View inflaterview= LayoutInflater.from(context).inflate(R.layout.baseadaper_proceedpayment, parent, false);
        ViewHolder vh=new ViewHolder(inflaterview);


        return vh;
    }

    @Override
    public void onBindViewHolder(final Baseadapter_Proceed.ViewHolder holder, final int position) {
        //      Picasso.with(context).load(listPic.get(position)).into(holder.productImage);
        holder.proceedBrand.setText(""+listBname.get(position));
        holder.proceedItem.setText(""+listVareity.get(position));
        holder.proceedNum.setText(""+listQaunt.get(position));
        holder.proceedBrand.setTypeface(typefaceClass.functionType(context));

        holder.proceedNum.setTypeface(typefaceClass.functionType(context));

        holder.proceedItem.setTypeface(typefaceClass.functionType(context));

        holder.imgEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {





                AlertDialog.Builder builder=new AlertDialog.Builder(context);
                builder.setTitle("Choose Action");
                builder.setMessage("What you want to do?");
                builder.setPositiveButton("Modify Order", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
                        Display metrics = windowManager.getDefaultDisplay();
                        int width = metrics.getWidth();
                        int height = metrics.getHeight();
                        Dialog dialogCustom = new Dialog(context);
                        dialogCustom.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                        dialogCustom.setContentView(R.layout.dialogaddcart);
                        dialogCustom.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        dialogCustom.getWindow().setLayout(width, height / 3);
                        TextView itemName = (TextView) dialogCustom.findViewById(R.id.cart_Itemname);
                        TextView itemVar = (TextView) dialogCustom.findViewById(R.id.cart_ItemVar);
                        final TextView totalPrice = (TextView) dialogCustom.findViewById(R.id.totalPrice);
                        itemName.setText("" + listBname.get(position));
                        itemVar.setText("" + listVareity.get(position));
                        final TextView numCount = (TextView) dialogCustom.findViewById(R.id.numCount);
                        numCount.setText(""+listQaunt.get(position));
                        countNum=Integer.parseInt(listQaunt.get(position));
                        getPrice=Integer.parseInt(listPrice.get(position));
                        total=getPrice*countNum;
                        totalPrice.setText(" ₹ "+total);
                        Button btnMinus = (Button) dialogCustom.findViewById(R.id.btnMinus);
                        Button btnPlus = (Button) dialogCustom.findViewById(R.id.btnPlus);
                        btnMinus.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if (countNum < 1) {

                                } else {
                                    countNum--;
                                    numCount.setText(" ₹ " + countNum);
                                    total = total - getPrice;


                                    totalPrice.setText(" ₹ " + total);

                                  //  totalPrice.setText("  ₹  " + dbCounter.TotalSum());

                                }
                            }
                        });


                        btnPlus.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                countNum++;
                                numCount.setText("" + countNum);
                                total = getPrice * countNum;

                                totalPrice.setText(" ₹ " + total);
                             //  totalPrice.setText("  ₹  " + dbCounter.TotalSum());

                            }
                        });



                          Button button=(Button)dialogCustom.findViewById(R.id.doneMilk);
                          button.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v)
                            {
                                dbCounter.insertIntoDatabase(listBname.get(position), listVareity.get(position), "" + countNum, listPrice.get(position), "" + total,"","");

                                notifyItemChanged(position);
                                dbCounter.SelectValue(listBname, listVareity, listQaunt, listPrice,listCat,layoutEmptyCart,layoutFilledCart);

    textView.setText(" ₹ " + dbCounter.TotalSum());
                                ColorChange();
                            }
                        });
                        dialogCustom.show();

//                    }


                    }


                });
                builder.setNegativeButton("Delete Order", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {




                            layoutFilledCart.setVisibility(View.VISIBLE);
                            layoutEmptyCart.setVisibility(View.GONE);
                            try {
                                dbCounter.DeleteItem(listBname.get(position), listVareity.get(position));
                                notifyItemRangeChanged(0, listBname.size());
                                dbCounter.SelectValue(listBname, listVareity, listQaunt, listPrice, listCat,layoutEmptyCart,layoutFilledCart);
//                        //  dbCounter.TotalSum(textView);
                                finalPrice = Integer.parseInt(dbCounter.TotalSum());
                                ColorChange();
                                textView.setText(" ₹ " + finalPrice);
                            }catch (Exception e)
                            {


                        }
                    }
                });


builder.create();
                builder.show();


            }
        });

    }

    @Override
    public int getItemCount() {
        return listBname.size();
    }


    public class ViewHolder extends  RecyclerView.ViewHolder
    {


        TextView proceedBrand;
        TextView proceedItem;
        TextView proceedNum;
        ImageView imgEdit;
        public ViewHolder(View itemView) {
            super(itemView);
imgEdit=(ImageView)itemView.findViewById(R.id.edit);

        proceedBrand=(TextView)itemView.findViewById(R.id.proceed_Brand);
        proceedItem=(TextView)itemView.findViewById(R.id.proceed_item);
            proceedNum=(TextView)itemView.findViewById(R.id.proceed_Num);
        }
    }

    public void ColorChange()
    {
        if(total>sharedPrefClass.getAccountBalance() || Integer.parseInt(dbCounter.TotalSum()) > sharedPrefClass.getAccountBalance())
        {
            textView.setTextColor(Color.RED);
        }
        else
        {
            textView.setTextColor(Color.BLACK);
        }
    }
}
