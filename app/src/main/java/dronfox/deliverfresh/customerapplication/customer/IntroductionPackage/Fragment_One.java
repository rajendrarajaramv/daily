package dronfox.deliverfresh.customerapplication.customer.IntroductionPackage;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import dronfox.deliverfresh.customerapplication.customer.R;
import dronfox.deliverfresh.customerapplication.customer.TypefaceClass;

/**
 * Created by Pardeep on 11/15/2015.
 */
public class Fragment_One extends Fragment
{
    TextView introTag;
    TextView introDesc;
    TypefaceClass typefaceClass;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

    View view=inflater.inflate(R.layout.intro_one,container,false);

        typefaceClass=new TypefaceClass();

     introDesc=(TextView)view.findViewById(R.id.introContent);
        introTag=(TextView)view.findViewById(R.id.introTag);

     introTag.setTypeface(typefaceClass.functionType(getActivity()));
        introDesc.setTypeface(typefaceClass.functionType(getActivity()));

        typefaceClass.setShodow(introTag);

        typefaceClass.setShodow(introDesc);
        return  view;

    }
}
