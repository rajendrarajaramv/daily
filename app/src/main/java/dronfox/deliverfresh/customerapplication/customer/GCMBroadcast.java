package dronfox.deliverfresh.customerapplication.customer;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import dronfox.deliverfresh.customerapplication.customer.SharedPrefrencesPackage.SharedPrefClass;

/**
 * Created by Pardeep on 11/12/2015.
 */
public class GCMBroadcast extends BroadcastReceiver
{

    SharedPrefClass sharedPrefClass;
MainActivity mainActivity;


    @Override
    public void onReceive(Context context, Intent intent) {
sharedPrefClass=new SharedPrefClass(context);
mainActivity=new MainActivity();
        String action = intent.getAction();
        Bundle bundle=intent.getExtras();

        if (action.equals("com.google.android.c2dm.intent.REGISTRATION")) {
            String reg_id = intent.getStringExtra("registration_id");
            String error = intent.getStringExtra("error");
            String unreg = intent.getStringExtra("unregistration");
////			Log.i("error", error);
        }
        else if (action.equals("com.google.android.c2dm.intent.RECEIVE"))
        {
          if(bundle.getString("action").equals("holidayVendor"))
          {
           makeNotification(context, "Vendor is on Holiday", "" + bundle.getString("message"));
          }

else if(bundle.getString("action").equals("orderDispatched"))
          {
              makeNotificationWithActions(context, "Order Dispatched", "" + bundle.getString("message"), "9779032164");
          }
          else if(bundle.getString("action").equals("RechargeDone"))
           {
               makeNotification(context,"Recharge Done","Your Requested Amount is added to your wallet,You can place the orders now");


               try
               {
                   mainActivity.getInstance().UpdateHeader();
               }catch (Exception e)
               {

               }
           }
          else if(bundle.getString("action").equals("orderDelete"))
            {

                makeNotification(context,"Refund","Refund of the deleted items are added to your wallet");


                try
                {
                    mainActivity.getInstance().UpdateHeader();
                }catch (Exception e)
                {

                }
            }
            else if(bundle.getString("action").equals("aproved"))
           {
               makeNotification(context, "Request Aproved", "Your Request is Aproved, You can Place Order now");
sharedPrefClass.setUservalid(true);

try
{
   mainActivity.getInstance().UpdateHeader();
}catch (Exception e)
{

}

           }

            else if(bundle.getString("action").equals("fetchBack")) {
            context.startService(new Intent(context,ServiceFetchItems.class));
          }

        }
    }

    public void makeNotification(Context context,String title,String msg)
    {


        Intent intents = new Intent(context,MainActivity.class);
        PendingIntent pIntent = PendingIntent.getActivity(context, (int) System.currentTimeMillis(), intents, 0);
        Notification.Builder n  = new Notification.Builder(context)
                .setContentTitle(title)
                .setContentText(msg)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentIntent(pIntent)
                .setStyle(new Notification.BigTextStyle().bigText(msg))
                .setDefaults(Notification.DEFAULT_LIGHTS | Notification.DEFAULT_SOUND).setVibrate(new long[]{1000,1000})
                .setAutoCancel(true);


        NotificationManager notificationManager = (NotificationManager)context. getSystemService(context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, n.build());
    }






    public void makeNotificationWithActions(Context context,String title,String msg,String contactNumber)
    {



Intent intDelivered=new Intent(context,OrderDel.class);
        PendingIntent penDel = PendingIntent.getActivity(context,0,intDelivered,0);



        Intent intents = new Intent(Intent.ACTION_CALL);
        intents.setData(Uri.parse("tel:"+contactNumber));
        PendingIntent pIntent = PendingIntent.getActivity(context,0,intents,0);
        Notification.Builder n  = new Notification.Builder(context)
                .setContentTitle(title)
                .setContentText(msg)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentIntent(pIntent)
                .setStyle(new Notification.BigTextStyle().bigText(msg))
                .addAction(R.drawable.tc,"Delivered",penDel);





        NotificationManager notificationManager = (NotificationManager)context. getSystemService(context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, n.build());
    }


}
