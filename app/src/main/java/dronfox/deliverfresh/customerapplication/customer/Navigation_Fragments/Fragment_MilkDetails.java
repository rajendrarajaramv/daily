package dronfox.deliverfresh.customerapplication.customer.Navigation_Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.util.ArrayList;

import dronfox.deliverfresh.customerapplication.customer.BaseAdapterPackage.Baseadapter_BradnName;
import dronfox.deliverfresh.customerapplication.customer.DatabaseFolder.DatabaseMilkType;
import dronfox.deliverfresh.customerapplication.customer.MainRetrofit;
import dronfox.deliverfresh.customerapplication.customer.R;
import dronfox.deliverfresh.customerapplication.customer.SharedPrefrencesPackage.SharedPrefClass;
import dronfox.deliverfresh.customerapplication.customer.ShowCaseClass;

/**
 * Created by Pardeep on 11/8/2015.
 */
public class Fragment_MilkDetails extends Fragment //implements View.OnClickListener
{


    String[] month = {"Jan", "Feb", "March", "April", "May", "June", "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"};

    Spinner spinner;
    ArrayList<String> brandVariety;
    ArrayList<String> brandPrice;
ArrayList<Integer> listCount;
    ArrayAdapter<String> itemTypeAdapter;
    ArrayList<String> typeList;
    DatabaseMilkType databaseMilkType;
    String getCategory;
    LinearLayoutManager linearLayoutManager;
    RecyclerView recyclerView;

    public static boolean checTool=false;

    String isUserTrue="";

    SharedPrefClass sharedPrefClass;

ArrayList<String> picPath;
MainRetrofit  mainRetrofit;

    ShowCaseClass showCaseClass;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.milkdetails, container, false);
mainRetrofit=new MainRetrofit();
        picPath=new ArrayList<String>();
        sharedPrefClass=new SharedPrefClass(getActivity());

showCaseClass=new ShowCaseClass(getActivity());

        LayoutInflater layoutInflater=(LayoutInflater)getActivity().getSystemService(getActivity().LAYOUT_INFLATER_SERVICE);
        View Spine=layoutInflater.inflate(R.layout.spinnerview,null);




        ((AppCompatActivity) getActivity()).getSupportActionBar().setCustomView(Spine);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowCustomEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("");



//
//retrofitInterface.CheckBalance(sharedPrefClass.getContactNumber(), new Callback<List<PojoClass>>() {
//    @Override
//    public void success(List<PojoClass> pojoClasses, Response response) {
//        for(int i=0;i<pojoClasses.size();i++)
//        {
//
//
//            sharedPrefClass.setAccountBalance(Integer.parseInt(pojoClasses.get(i).getAcountBalance()));
//        isUserTrue=pojoClasses.get(i).getIsUserTrue();
//
//        }
//    }
//
//    @Override
//    public void failure(RetrofitError retrofitError) {
//
//    }
//});
//


        typeList=new ArrayList<String>();
        brandVariety=new ArrayList<String>();
        brandPrice=new ArrayList<String>();
listCount=new ArrayList<Integer>();
        linearLayoutManager=new LinearLayoutManager(getActivity());
        recyclerView=(RecyclerView)view.findViewById(R.id.recyclerviewRate);
        recyclerView.setLayoutManager(linearLayoutManager);
setHasOptionsMenu(true);
      getCategory=getArguments().getString("category");
        databaseMilkType=new DatabaseMilkType(getActivity());
        databaseMilkType.getBrandName(getCategory,typeList);
        spinner = (Spinner)Spine.findViewById(R.id.spin);
        itemTypeAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item,typeList);
        spinner.setAdapter(itemTypeAdapter);



        showCaseClass.ShowCase(spinner,getActivity(),"Close","Change the Brands from here","0");
       spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
           @Override
           public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

           databaseMilkType.getBrandVariety(getCategory,"" + spinner.getSelectedItem(), brandVariety, brandPrice,picPath);
               recyclerView.setAdapter(new Baseadapter_BradnName(getActivity(),brandVariety,brandPrice,listCount,""+spinner.getSelectedItem(),getCategory,picPath));

         if(isUserTrue.equalsIgnoreCase("pending"))
               {


               }


           }

           @Override
           public void onNothingSelected(AdapterView<?> parent) {

           }
       });

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//        menu.clear();
//        inflater.inflate(R.menu.actoncart,menu);







        super.onCreateOptionsMenu(menu, inflater);
    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//    switch (item.getItemId())
//    {
//        case R.id.cart:
//
//            break;
//    }
//        return super.onOptionsItemSelected(item);
    }
