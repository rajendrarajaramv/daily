package dronfox.deliverfresh.customerapplication.customer;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import dronfox.deliverfresh.customerapplication.customer.BaseAdapterPackage.Baseadapter_Proceed;
import dronfox.deliverfresh.customerapplication.customer.DatabaseFolder.DbCounter;
import dronfox.deliverfresh.customerapplication.customer.SharedPrefrencesPackage.SharedPrefClass;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Pardeep on 11/22/2015.
 */
public class ProceedPayment  extends Fragment implements View.OnClickListener {


    String[] months={"Jan","Feb","March","April","May","Jun","Jul","Aug","Sept","Oct","Nov","Dec"};

    int grandSum=0;
    boolean alow=false;
    ArrayList<String> listBname;
    ArrayList<String> listVareity;
    ArrayList<String> listQaunt;
    ArrayList<String> grandTotal;
    ArrayList<String> itemPrice;
TextView type;
    TextView startDate;
    TextView endDate;
    ImageView calander;

    TableRow layoutCal;

    MainActivity mainActivity;


boolean mode=false;
int diffrenctOfDay;

String isUserTrue="false";


ArrayList<String> sendBname;

    ArrayList<String> sendBVer;

    ArrayList<String> sendQaunt;

   ArrayList<String> sendPrice;
    ArrayList<String> sendCat;

String isRgular="Tomorrow Only";

//    listBname.clear();
//    listVareity.clear();
//    listQaunt.clear();
//    listPrice.clear();

   public static String choosedDate="";


boolean canIOrder=false;
boolean isUserExists;
    String ontherDate="";

    LinearLayout filledCart;
    LinearLayout emptyCart;
Date otherChoosedDate;

    int totalIfRegular;

TextView textView;
    RecyclerView recyclerView;
    LinearLayoutManager linearLayoutManager;
    DbCounter dbCounter;
Button paymenrButton;
    ImageView edit_Type;
  public static   int finalTotal;
   SharedPrefClass sharedPrefClass;
    Date date_Today;
    Date date_Choosed;
   MainRetrofit mainRetrofit;
ShowCaseClass showCaseClass;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.proceedpayment, container, false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowCustomEnabled(false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Proceed");
        sharedPrefClass=new SharedPrefClass(getActivity());
    mainActivity=new MainActivity();
        mainRetrofit=new MainRetrofit();
        type=(TextView)view.findViewById(R.id.type);
        startDate=(TextView)view.findViewById(R.id.startDate);
        endDate=(TextView)view.findViewById(R.id.endDate);
        calander=(ImageView)view.findViewById(R.id.calander);
        layoutCal=(TableRow)view.findViewById(R.id.layoutCal);
        layoutCal.setVisibility(View.GONE);


        showCaseClass=new ShowCaseClass(getActivity());
        Calendar calendar = Calendar.getInstance();
        Date today = calendar.getTime();
        calendar.add(Calendar.DAY_OF_YEAR, 1);
        Date tomorrow = calendar.getTime();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String tomorrowAsString = dateFormat.format(tomorrow);

//        retrofitInterface.checkValidation(sharedPrefClass.getContactNumber(), new Callback<List<PojoClass>>() {
//            @Override
//            public void success(List<PojoClass> pojoClasses, Response response) {
//                for(int i=0;i<pojoClasses.size();i++)
//                {
//                    isUserTrue=pojoClasses.get(i).getIsUserTrue();
//                }
//
//
//
//                if(isUserTrue.equalsIgnoreCase("Pending"))
//                {
//                    isUserExists=false;
//                }
//                else
//                {
//                    isUserExists=true;
//                }
//
//
//            }
//
//            @Override
//            public void failure(RetrofitError retrofitError) {
//
//            }
//        });



mainRetrofit.functionRetro().canIPost(sharedPrefClass.getContactNumber(), tomorrowAsString, new Callback<String>() {
    @Override
    public void success(String s, Response response) {


        if (s.equals("cannot order")) {
            canIOrder = false;
        } else {
            canIOrder = true;
        }



    }

    @Override
    public void failure(RetrofitError retrofitError) {

    }
});



        sendCat=new ArrayList<String>();
        sendBname=new ArrayList<String>();
        sendBVer=new ArrayList<String>();
        sendQaunt=new ArrayList<String>();
        sendPrice=new ArrayList<String>();
        edit_Type=(ImageView)view.findViewById(R.id.edit_OrderType);

        showCaseClass.ShowCase(edit_Type,getActivity(),"Close","On Daily Basis You will get order for one day only, and on regular basis you will get same order atleast for 5 days","3");

        listBname=new ArrayList<String>();
        listVareity=new ArrayList<String>();
        listQaunt=new ArrayList<String>();
        grandTotal=new ArrayList<String>();
        itemPrice=new ArrayList<String>();
        textView=(TextView)view.findViewById(R.id.textView);
        linearLayoutManager=new LinearLayoutManager(getActivity());
        recyclerView=(RecyclerView)view.findViewById(R.id.recyclerCartItems);
        recyclerView.setLayoutManager(linearLayoutManager);
        dbCounter=new DbCounter(getActivity());


        filledCart=(LinearLayout)view.findViewById(R.id.filledCart);
        emptyCart=(LinearLayout)view.findViewById(R.id.emptyCart);

        if(dbCounter.ReturnSize()<1)
        {
            emptyCart.setVisibility(View.VISIBLE);

            filledCart.setVisibility(View.GONE);
        }
        else
        {

            emptyCart.setVisibility(View.GONE);
            filledCart.setVisibility(View.VISIBLE);
        }






        dbCounter.SelectValue(listBname, listVareity, listQaunt,itemPrice,sendCat,emptyCart,filledCart);
        recyclerView.setAdapter(new Baseadapter_Proceed(getActivity(), listBname, listVareity, listQaunt, grandTotal, textView,itemPrice,sendCat,filledCart,emptyCart));
try {
    finalTotal = Integer.parseInt(dbCounter.TotalSum());
}catch (Exception  e)
{
    finalTotal=0;
}
        if(finalTotal > sharedPrefClass.getAccountBalance()) {
textView.setTextColor(Color.RED);
            alow=false;
        }
        else {
            textView.setTextColor(Color.BLACK);

        alow=true;
        }
        textView.setText(" ₹ " + finalTotal);
        paymenrButton=(Button)view.findViewById(R.id.paymenrButton);

        calander.setOnClickListener(this);


        paymenrButton.setOnClickListener(this);

        edit_Type.setOnClickListener(this);


        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    dbCounter.UpdateDays("1");
                    getFragmentManager().popBackStackImmediate();

                }
                return false;
            }
        });


        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if(keyCode==KeyEvent.KEYCODE_BACK)
                {
                    getFragmentManager().popBackStackImmediate();
                }
                return false;
            }
        });

        return view;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.calander:
                final Dialog dialogCalander=new Dialog(getActivity());
               dialogCalander.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                dialogCalander.setContentView(R.layout.calenderdialog);
                final MaterialCalendarView calendarView=(MaterialCalendarView)dialogCalander.findViewById(R.id.calendarView);
                calendarView.setMinimumDate(new Date());
                calendarView.setOnDateChangedListener(new OnDateSelectedListener() {
                    @Override
                    public void onDateSelected(MaterialCalendarView widget, CalendarDay date, boolean selected) {
                        Calendar calendar=Calendar.getInstance();
diffrenctOfDay=0;


                        String todayDate=new SimpleDateFormat("dd-MM-yyyy").format(new Date());
                        int month=date.getMonth()+1;
                        DecimalFormat mFormat= new DecimalFormat("00");
                        mFormat.format(month);
                         choosedDate=mFormat.format(date.getDay())+"-"+mFormat.format(month)+"-"+date.getYear();
                                            DateFormat dateFormat=new SimpleDateFormat("dd-MM-yyyy");
                        try {
                            date_Today   = dateFormat.parse(todayDate);
                            date_Choosed    =dateFormat.parse(choosedDate);

                        }catch (Exception e)
                        {
                        }
                        if(daysBetween(date_Today,date_Choosed) < 5)
                        {
                            Snackbar.make(getActivity().findViewById(android.R.id.content), "Order Shall Be More Then 5 Days", Snackbar.LENGTH_LONG).show();

                        }
                        else
                        {



                            diffrenctOfDay=daysBetween(date_Today,date_Choosed);
                            endDate.setText(""+date.getDay()+"\n"+months[date.getMonth()]);
                           // finalTotal=finalTotal*diffrenctOfDay;
dbCounter.UpdateDays(""+diffrenctOfDay);

                           if (Integer.parseInt(dbCounter.TotalSum())> sharedPrefClass.getAccountBalance())
                           {
                               textView.setTextColor(Color.RED);
                           }
                            else
                           {
                               textView.setTextColor(Color.BLACK);
                           }
                            textView.setText(" ₹ "+dbCounter.TotalSum());


dialogCalander.dismiss();
                        }
                    }
                });


                dialogCalander.show();












                break;
            case R.id.edit_OrderType:
                AlertDialog.Builder builderType=new AlertDialog.Builder(getActivity());
                builderType.setTitle("Choose Order Placement Type");
                builderType.setMessage("Do you want same order everyday?");
                builderType.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        type.setText("Everyday");

                        showCaseClass.ShowCase(calander,getActivity(),"Close","Set the dates for which you want the orders everyday","74");


                        layoutCal.setVisibility(View.VISIBLE);
                        Calendar calendar = Calendar.getInstance();
                        Date today = calendar.getTime();
                        calendar.add(Calendar.DAY_OF_YEAR, 1);
                        Date tomorrow = calendar.getTime();
                        DateFormat dateFormat = new SimpleDateFormat("dd\nMMM");
                        String tomorrowAsString = dateFormat.format(tomorrow);
                        startDate.setText(tomorrowAsString);
//                        textView.setText(""+dbCounter.TotalSum());
isRgular="Regular";


                        mode=true;



                    }
                });

                builderType.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        type.setText("Just Tomorrow");
                        layoutCal.setVisibility(View.GONE);
                        endDate.setText("--");
                        textView.setText("  ₹ "+dbCounter.TotalSum());
mode=false;
                        isRgular="Non Regular";
dbCounter.UpdateDays("1");
choosedDate="";
                        if(Integer.parseInt(dbCounter.TotalSum()) > sharedPrefClass.getAccountBalance())
                        {
                            textView.setTextColor(Color.RED);
                        }
                        else
                        {
                            textView.setTextColor(Color.BLACK);
                        }
                        textView.setText(" ₹ " + dbCounter.TotalSum());
                    }
                });
                builderType.create();
                builderType.show();
                break;


            case R.id.paymenrButton:
                if(sharedPrefClass.isUservalid()==false)
                {
                    Snackbar.make(getActivity().findViewById(android.R.id.content),"Cannot Place Order Untill You Are Varified by Vendor",Snackbar.LENGTH_LONG).show();
                }




                else if(canIOrder==false)
{
    AlertDialog.Builder builderRecharge = new AlertDialog.Builder(getActivity());
    builderRecharge.setTitle("Cannot Place Order");
    builderRecharge.setMessage("You have already place a Regular Order, So You Cannot place order until the date complete");
    builderRecharge.setPositiveButton("Cancel", new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
        }
    });

    builderRecharge.create();
    builderRecharge.show();

}



    else
                        if(Integer.parseInt(dbCounter.TotalSum()) > sharedPrefClass.getAccountBalance()) {
                            AlertDialog.Builder builderRecharge = new AlertDialog.Builder(getActivity());
                            builderRecharge.setTitle("Insufficient Balance");
                            builderRecharge.setMessage("Your Account Balance is insuffient to place this order,Please Recharge");
                            builderRecharge.setPositiveButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            });

                            builderRecharge.setNegativeButton("Recharge", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            });
                            builderRecharge.create();
                            builderRecharge.show();
                        }
                else {


                            Calendar calendar = Calendar.getInstance();
                            Date today = calendar.getTime();
                            calendar.add(Calendar.DAY_OF_YEAR, 1);
                            Date tomorrow = calendar.getTime();
                            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                            String tomorrowAsString = dateFormat.format(tomorrow);
                            try {
                                choosedDate = dateFormat.format(date_Choosed);
                            } catch (Exception e) {
                                choosedDate = "";
                            }
                            dbCounter.SelectValue(sendBname, sendBVer, sendQaunt, sendPrice, sendCat, emptyCart, filledCart);

                            String timestamp = new SimpleDateFormat("ddMMyyyy_HHmmss").format(new Date());

                            grandSum = Integer.parseInt(dbCounter.getNumdays()) * 4;

                               //grandSum = Integer.parseInt(dbCounter.getNumdays()) * 3.33;




//Snackbar.make(getActivity().findViewById(android.R.id.content),"₹ "+grandSum+" will be charged extra due to vat and tex",Snackbar.LENGTH_LONG).show();

int getTotalSum=Integer.parseInt(dbCounter.TotalSum());



                            if(getTotalSum > sharedPrefClass.getAccountBalance())
                            {
                                Snackbar.make(getActivity().findViewById(android.R.id.content),"Cannot Place Order Due to low Balance, Balance After Vat and Text include",Snackbar.LENGTH_LONG).show();

                            }
                            else {



                                int cutAmount=sharedPrefClass.getAccountBalance()-getTotalSum;



                                ProgressDialog progressDialog=new ProgressDialog(getActivity());
                                progressDialog.setMessage("Placing Order");
                                progressDialog.show();

                                for (int i = 0; i < sendBname.size(); i++) {


                                    mainRetrofit.functionRetro().OrdersPlace(timestamp, sharedPrefClass.getContactNumber(), "" + tomorrowAsString, "" + choosedDate,
                                            sendCat.get(i), "Pending", sharedPrefClass.getPlace(),
                                            "" + sendBname.get(i), sendBVer.get(i), sendQaunt.get(i), isRgular, new Callback<String>() {
                                                @Override
                                                public void success(String s, Response response) {
                                                    if (s.equals("placed")) {

                                                        try
                                                        {
                                                            MainActivity.getInstance().UpdateHeader();
                                                        }catch (Exception e)
                                                        {

                                                        }


                                                    }
                                                }

                                                @Override
                                                public void failure(RetrofitError retrofitError) {

                                                }
                                            });
                                }

progressDialog.dismiss();



//
                                mainRetrofit.functionRetro().cutBalnce(sharedPrefClass.getContactNumber(), "" + getTotalSum, new Callback<List<PojoClass>>() {
                                    @Override
                                    public void success(List<PojoClass> pojoClasses, Response response) {

                                    }

                                    @Override
                                    public void failure(RetrofitError retrofitError) {

                                    }
                                });




                                sharedPrefClass.setAccountBalance(cutAmount);

                                dbCounter.Delete();
                                dbCounter.SelectValue(listBname, listVareity, listQaunt, itemPrice, sendCat, emptyCart, filledCart);


                                AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());
                                builder.setTitle("Order Placed");
                                builder.setMessage("Your Order is successfully placed");
                                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                });
                                builder.create();
                                builder.show();


try {
    mainActivity.getInstance().UpdateHeader();
}catch (Exception e)
{

}

                            }
                        }












                break;



        }
    }

    public int daysBetween(Date d1, Date d2){
        return (int)( (d2.getTime() - d1.getTime()) / (1000 * 60 * 60 * 24));
    }
}
