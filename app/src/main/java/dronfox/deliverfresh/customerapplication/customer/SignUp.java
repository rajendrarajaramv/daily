package dronfox.deliverfresh.customerapplication.customer;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.util.ArrayList;
import java.util.List;

import dronfox.deliverfresh.customerapplication.customer.DatabaseFolder.CityRecords;
import dronfox.deliverfresh.customerapplication.customer.JsonLocation.PojoClassLocation;
import dronfox.deliverfresh.customerapplication.customer.SharedPrefrencesPackage.SharedPrefClass;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Pardeep on 11/7/2015.
 */
public class SignUp extends AppCompatActivity implements View.OnClickListener {
    ArrayList<String> choosePlace;
    public static String getGcmId;
    public static String projectNumber = "60095088152";

    MainRetrofit mainRetrofit;


    TelephonyManager telephonyManager;
    GoogleCloudMessaging googleCloudMessaging;

    TextView locationString;
    GPSTracker gpsTracker;

    SharedPrefClass sharedPrefClass;
    Spinner spinnerArea;
    Spinner spinnerCity;


    Button proceed;


    ArrayAdapter<String> adapterCity;
    ArrayAdapter<String> adapterArea;

String getPlace;
    String getPlaceArea;

    ArrayList<String> listCity;
    ArrayList<String> listArea;
    String getLocationString="Not Available";
    CityRecords cityRecords;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup);
        sharedPrefClass=new SharedPrefClass(this);
        spinnerArea = (Spinner) findViewById(R.id.spinnerArea);
        spinnerCity = (Spinner) findViewById(R.id.spinnerCity);
        gpsTracker = new GPSTracker(this);
        listCity = new ArrayList<String>();
        listArea = new ArrayList<String>();
        mainRetrofit = new MainRetrofit();

        googleCloudMessaging=GoogleCloudMessaging.getInstance(this);
        new AsynGcm().execute();
        proceed = (Button) findViewById(R.id.proceed);
        proceed.setOnClickListener(this);
        locationString = (TextView) findViewById(R.id.locationString);

        locationString.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(getLocationString.equals(null) || getLocationString==null)
                {
                    locationString.setText("We Are Unable To find location");
                }
                else {

                    locationString.setText(""+getLocationString);
                }
            }
        });


        adapterArea = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, listArea);

        adapterCity = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, listCity);

        cityRecords = new CityRecords(this);
        try {
            cityRecords.Delete();
        } catch (Exception e) {

        }

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Fetching Records");
        progressDialog.show();
        mainRetrofit.functionRetro().FetchAllPlace(new Callback<List<PojoClass>>() {
            @Override
            public void success(List<PojoClass> pojoClasses, Response response) {
                for (int i = 0; i < pojoClasses.size(); i++) {
                    cityRecords.insertDb(pojoClasses.get(i).getCityName(), pojoClasses.get(i).getPlaceName());
                }

                cityRecords.Select(listCity);

                spinnerCity.setAdapter(adapterCity);
                progressDialog.dismiss();
            }

            @Override
            public void failure(RetrofitError retrofitError) {

            }
        });
        spinnerCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                cityRecords.SeelctArea(listArea, listCity.get(position));

                getPlace = listCity.get(position);


                spinnerArea.setAdapter(adapterArea);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerArea.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                getPlaceArea=listArea.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

try {
    RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint("http://maps.googleapis.com").build();
    restAdapter.setLogLevel(RestAdapter.LogLevel.FULL);
    RetrofitInterface retrofitInterface = restAdapter.create(RetrofitInterface.class);
    String latlong = "" + gpsTracker.getLatitude() + "," + gpsTracker.getLongitude();
    retrofitInterface.FetchLocationGoogle(latlong, "true", new Callback<PojoClassLocation>() {
        @Override
        public void success(PojoClassLocation pojoClassLocation, Response response) {
            try {

                getLocationString = "" + pojoClassLocation.getResults().get(0).getFormatted_address();
                //locationString.setText("" + getLocationString);
                //  progressDialog.dismiss();

            } catch (Exception e) {
                getLocationString = "Not Found";
            }
        }

        @Override
        public void failure(RetrofitError retrofitError) {

        }
    });

}catch (Exception e)
{

}
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.proceed:


//                WindowManager windowManager = (WindowManager)getSystemService(WINDOW_SERVICE);
//                Display display = windowManager.getDefaultDisplay();
//                final int height = display.getHeight() / 2;


                Dialog dialog = new Dialog(this);
                dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));



                dialog.setContentView(R.layout.enterinformation);
                dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);

            TelephonyManager telephonyManager=(TelephonyManager)getSystemService(TELEPHONY_SERVICE);


                final EditText edtAddress = (EditText) dialog.findViewById(R.id.edtAddress);

                edtAddress.setHint("your address in "+getPlaceArea);

                final EditText edt_name = (EditText) dialog.findViewById(R.id.edt_Name);
                final EditText edt_contact = (EditText) dialog.findViewById(R.id.edt_contact);
               edt_contact.setText(""+telephonyManager.getLine1Number());
               ImageView imageView=(ImageView)dialog.findViewById(R.id.markerIcon);
                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        edtAddress.setText("" + getLocationString);

                    }
                });
                final EditText edt_password = (EditText) dialog.findViewById(R.id.edt_Password);
                Button buttonRegister = (Button) dialog.findViewById(R.id.register);
                buttonRegister.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        if (edt_name.getText().toString().equals("")) {
                            edt_name.setError("Enter Name");
                        } else if (edt_contact.getText().toString().equals("") || edt_contact.getText().toString().length() <10 ) {
                            edt_contact.setError("Enter Contact");
                        } else if (edt_password.getText().toString().equals("")) {
                            edt_password.setError("Enter Password");
                        } else if (edtAddress.getText().toString().equals("")) {
                            edtAddress.setError("Enter Address");
                        } else {




                            final ProgressDialog progressDialog = new ProgressDialog(SignUp.this);
                            progressDialog.setMessage("Please Wait..");
                            progressDialog.show();
                            mainRetrofit.functionRetro().InsertRegistration(edt_name.getText().toString(), edt_contact.getText().toString(),
                                    getPlace, getGcmId, edt_password.getText().toString(),
                                    "Pending", edtAddress.getText().toString(), "0", getPlaceArea, new Callback<String>() {
                                        @Override
                                        public void success(String s, Response response) {

                                            if (s.equals("done")) {

sharedPrefClass.setAddress(edtAddress.getText().toString());
                                                sharedPrefClass.setPassword(edt_password.getText().toString());
                                                sharedPrefClass.setName(edt_name.getText().toString());
                                                sharedPrefClass.setPlace(getPlaceArea);
                                                sharedPrefClass.setState(getPlace);
                                                sharedPrefClass.setContactNumber(edt_contact.getText().toString());
                                                sharedPrefClass.setLogin("true");
                                                ShowDialogReg("Succuss", "Successfully Registered,But you cannot place order until vendor verify you identity", true, R.drawable.tickgreen);

                                            } else {
                                                ShowDialogReg("Signup Problem", "It seems like user is already exists on the server, Please try to login", false, R.drawable.tickred);
                                            }
                                            //  progressDialog.dismiss();
                                            progressDialog.dismiss();
                                        }

                                        @Override
                                        public void failure(RetrofitError retrofitError) {
                                            progressDialog.dismiss();
                                        }
                                    });


                        }


                    }
                });


                dialog.show();


                break;
        }
    }


    public class AsynGcm extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {

            try {
                getGcmId = googleCloudMessaging.register(projectNumber);
            } catch (Exception e)

            {
            }

            return null;
        }


    }




    public void ShowDialogReg(String title, String msg, final boolean b, int id) {

        final Dialog dialog = new Dialog(SignUp.this);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialogscreen);

        TextView textView = (TextView) dialog.findViewById(R.id.textSuccuss);
        TextView textMessage = (TextView) dialog.findViewById(R.id.textMessage);
        ImageView imageView = (ImageView) dialog.findViewById(R.id.imgTick);
        textView.setText("" + title);
        textMessage.setText("" + msg);


//        textView.setTypeface(typefaceClass.functionType(this));
//        textMessage.setTypeface(typefaceClass.functionType(this));


        imageView.setImageResource(id);

        Button button = (Button) dialog.findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (b == true) {
//                    sharedPrefClass.setLogin("true");
//                    sharedPrefClass.setContactNumber(edtContact.getText().toString());

                    Intent intent = new Intent(SignUp.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                } else {


                    Intent intent = new Intent(SignUp.this, Login.class);
                    startActivity(intent);
                    finish();

                    dialog.dismiss();
                }
            }
        });
//
//
        dialog.show();
    }





}