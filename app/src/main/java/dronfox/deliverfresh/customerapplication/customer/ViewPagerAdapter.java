package dronfox.deliverfresh.customerapplication.customer;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import dronfox.deliverfresh.customerapplication.customer.ViewPagerFragments.History;
import dronfox.deliverfresh.customerapplication.customer.ViewPagerFragments.UpcomingOrders;

/**
 * Created by Pardeep on 11/8/2015.
 */
public class ViewPagerAdapter extends FragmentStatePagerAdapter
{
    String[] title={"History","Upcoming Orders"};

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {


       switch (position)
       {
           case 0:
               return new History();
           case 1:

           return  new UpcomingOrders();
       }

        return null;
    }

    @Override
    public int getCount() {
        return 2;

    }


    @Override
    public CharSequence getPageTitle(int position) {
        return title[position];
    }
}
